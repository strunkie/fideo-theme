<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once __DIR__ . "/../../classes/gump/gump.class.php";
require_once __DIR__ . "/../../classes/phpmailer/src/Exception.php";
require_once __DIR__ . "/../../classes/phpmailer/src/PHPMailer.php";

$fideo_options = get_option( 'fideo_option_name' );

/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */
function jobapply_form( $request ) {
    $validator = new GUMP('nl');

    $params = $request->get_params();
    $files = $request->get_file_params();

    $success = $validator->validate($params, array(
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|valid_email',
        'phonenumber' => 'required|phone_number',
        'motivation' => 'required',
        'av' => 'required'
    ));

    if($success === true) {
        $mail = new PHPMailer(true); 

        try {
            ob_start();

            include(__DIR__ . "/mailtemplate.php");

            $file_content = ob_get_contents();

            ob_end_clean();

            $fideo_options = get_option( 'fideo_option_name' );

            $mail->setFrom($fideo_options['email_vacancy'], 'Fideo website');
            $mail->addAddress($fideo_options['email_vacancy'], 'Fideo');     // Add a recipient
            $mail->addReplyTo($params['email'], $params['name']);
            
            if(!empty($files) && !empty($files['attachment']) && !empty($files['attachment']['tmp_name'])) {
                $mail->addAttachment($files['attachment']['tmp_name'], $files['attachment']['name']);
            }

            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Fideo sollicitatie';
            $mail->Body    = $file_content;

            $mail->send();

            return array(
                "success" => true,
                "url" => '/bedankt-sollicatie/'
            );
        }
        catch (Exception $e) {
            return array(
                "success" => false,
                "message" => $e
            );
        }
    } else {
        $errors = array();

        foreach($validator->get_errors_array() as $field => $error) {
            $errors[] = array(
                "message" => $error,
                "name" => $field,
                "inputName" => $field
            );
        }

        return array(
            "success" => false,
            "errors" => $errors
        );
    }
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'fideo/v1', '/jobapply', array(
    'methods' => 'POST',
    'callback' => 'jobapply_form',
  ) );
} );