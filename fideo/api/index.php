<?php
include('newsletter.php');
include('contact/index.php');
include('jobapply/index.php');

function pages_by_type( $request ) {
	$params = $request->get_params();

 	$args = array( 'post_type' => $params['type'], 'posts_per_page' => -1 );
    $loop = new WP_Query( $args );
    $items = array();
    while ( $loop->have_posts() ) : $loop->the_post();

    	$items[] = array(
    		id => get_post()->ID,
    		title => get_post()->post_title
    	);

    endwhile;

    return $items;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'fideo/v1', '/pages_by_type', array(
    'methods' => 'GET',
    'callback' => 'pages_by_type',
  ) );
} );