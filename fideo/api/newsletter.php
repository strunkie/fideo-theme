<?php
include(__DIR__ . '/../classes/MailChimp.php');

use \DrewM\MailChimp\MailChimp;

/**
 * Grab latest post title by an author!
 *
 * @param array $data Options for the function.
 * @return string|null Post title for the latest,  * or null if none.
 */
function newsletter_subscribe( $data ) {
    $parameters = $data->get_params();

    $errors = [];

    if(empty($parameters['email'])) {
        $errors[] = array(
            'inputName' => 'email',
            'message' => 'Emailadres bestaat al of is niet geldig.'
        );
    }


    if(!empty($parameters['email']) && !filter_var($parameters['email'], FILTER_VALIDATE_EMAIL)) {
        $errors[] = array(
            'inputName' => 'email',
            'message' => 'Emailadres is niet geldig.'
        );
    }

    if(empty($parameters['privacy'])) {
        $errors[] = array(
            'inputName' => 'privacy',
            'message' => 'Je moet akkoord gaan met het privacy statement'
        );
    }

    if(count($errors) > 0) {
        return array(
            'errors' => $errors
        );
    }


    $MailChimp = new MailChimp('ee3807058a81050a0472c1a54b408dd6-us9');

    // $result = $MailChimp->get('lists');

    // print_r($result);

    $result = $MailChimp->post('lists/d101c80bc5/members', [
        'email_address' => $parameters['email'],
        'status' => 'subscribed'
    ]);

    if($result['status'] === 400) {
        return array(
            'errors' => array(
                array(
                    'inputName' => 'email',
                    'message' => 'Emailadres bestaat al of is niet geldig.'
                )
            )
        );
    }

    return array(
        'success' => true,
    );
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'fideo/v1', '/newsletter', array(
    'methods' => 'POST',
    'callback' => 'newsletter_subscribe',
  ) );
} );