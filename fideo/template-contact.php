<?php /* Template Name: Contact template */ get_header(); ?>
<section class="contact">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-24 medium-15 large-14 contact__content bg--dark-blue">
                <div class="contact__content__inner">

                    <h1 class="h4">Hallo Fideo!</h1>
                    
                    <div class="grid-x grid-padding-x">
                        <div class="cell small-24 medium-18 large-12">
                            <form class="form form--bg-blue" method="post" action="/wp-json/fideo/v1/contact/" id="form1" novalidate ajaxform>
                                <div class="grid-x grid-padding-x">
                                    <div class="cell small-24">
                                        <div class="formfield formfield--input">
                                            <label class="formfield__label">Mijn naam is:</label>
                                        
                                            <input type="text" placeholder="naam..." name="name" fieldID="name">

                                            <ul class="form__errors" data-errorgroup="name"> </ul>
                                        </div>
                                    </div>

                                    <div class="cell small-24">
                                        <div class="formfield formfield--input">
                                            <label class="formfield__label">Mijn emailadres is:</label>
                                        
                                            <input type="text" placeholder="emailadres..." name="email" fieldID="email">

                                            <ul class="form__errors" data-errorgroup="email"> </ul>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="grid-x grid-padding-x">
                                    <div class="cell small-24">
                                        <div class="formfield formfield--input">
                                            <label class="formfield__label">Mijn telefoonnummer is:</label>
                                        
                                            <input type="text" placeholder="telefoonnummer..." name="phonenumber" fieldID="phonenumber">

                                            <ul class="form__errors" data-errorgroup="phonenumber"> </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="grid-x grid-padding-x">
                                    <div class="cell small-24">
                                        <div class="formfield formfield--input">
                                            <label class="formfield__label">bel mij terug over...</label>
                                        
                                            <input type="text" placeholder="bel mij terug over..." name="subject" fieldID="subject">

                                            <ul class="form__errors" data-errorgroup="subject"> </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="formfield formfield--button">
                                    <button>
                                        <div class="btn">
                                            <span>Bel mij terug</span>
                                            <i class="icon-arrow-right"></i>
                                        </div>
                                    </button>
                            
                                    <span class="loader loader--white"></span>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="cell small-24 medium-9 large-10 contact__content contact__content--aside">
                <div class="contact__content__inner contact__address">
                    
                    <h3 class="h2">Fideo</h3>
                    <address>
                        Schuttevaerkade 80
                        <br />8021 DB Zwolle
                        <div class="icon-text icon-text--top-spacing">
                            <i class="icon-phone"></i><span><a href="tel:+038 458 10 75‬">038 458 10 75‬</a></span>
                        </div>
                        <div class="icon-text">
                            <i class="icon-email"></i><a href="mailto:info@fideo.nl" class="btn-text btn-text--white">info@fideo.nl</a>
                        </div>
                    </address>

                    <h3 class="h2">Appen</h3>
                    <p>
                        Wil je met ons appen? Dat kan! Stuur je bericht via WhatsApp om een chat te starten.
                    </p>

                    <div class="icon-text icon-text--top-spacing">
                        <i class="icon-whatsapp"></i><a href="tel:06 188 693 31" class="btn-text btn-text--white">06 188 693 31</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    <?php the_content(); // Dynamic Content ?>
<?php endwhile; ?>
<?php endif; ?>

<?php
global $nofooter;
$nofooter = true;

get_footer(); ?>