<?php
    $doelgroepen = wp_get_post_terms(get_the_ID(), 'doelgroep', array("fields" => "all"));
    $categories = wp_get_post_terms(get_the_ID(), 'category', array("fields" => "all"));
    $video = get_field('highlight_afbeelding_gif')['url'];
?>

<?php 
global $size;

if(!$size) {
    $size = 'small-24 medium-12';
}
?>
<a class="cell article <?php echo $size ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" <?php if($video) { ?>hover-video<?php } ?>>
    <figure class="article__figure">
        <?php if($video) { ?>
        <div class="article__figure-video">
            <video width="720" height="480" loop muted>
                <source src="<?php echo get_field('highlight_afbeelding_gif')['url'] ?>" type="video/mp4">
            </video>
        </div>
        <?php } ?>

        <img class="article__figure__image" src="<?php echo get_field('highlight_afbeelding_static')['sizes']['overview-image'] ?>" alt="<?php the_title(); ?>">

        <div class="article__figure__inner">
            <div>
            	<div class="article__play play">
            		<div class="play__btn">
            		    <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/play.png" alt="">
            		</div>
            		<div class="play__label label"><?php echo $categories[0]->name ?></div>
            	</div>
            	<div class="article__tag"><?php echo $doelgroepen[0]->name ?></div>
            </div>
        </div>
    </figure>

    <?php if(get_field('overview_subtitle')) : ?>
        <div class="article__undertitle"><?php echo get_field('overview_subtitle'); ?></div>
    <?php endif; ?>

    <h3 class="h2 article__title"><?php echo get_field('alternative_title') ?: the_title(); ?> </h3>
</a>