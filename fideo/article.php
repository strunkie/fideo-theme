<a class="cell article" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" hover-gif>
    <figure class="article__figure">
        <img class="article__figure__image" src="<?php echo get_field('highlight_afbeelding_static')['sizes']['overview-image'] ?>" gif-url="<?php echo get_field('highlight_afbeelding_gif')['sizes']['overview-image'] ?>" alt="">

        <div class="article__figure__inner">
            <div>
            	<div class="article__play play">
            		<div class="play__btn">
            		    <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/play.png" alt="">
            		</div>
            		<div class="play__label label">corporate video</div>
            	</div>
            	
            	<div class="article__tag">doelgroep</div>
            </div>
        </div>
    </figure>
    <span class="article__undertitle">Klant</span>
    <h3 class="h2 article__title"><?php the_title(); ?></h3>
</a>