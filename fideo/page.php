<?php
global $whiteheader;

$whiteheader = true;

get_header(); ?>
	<header class="header header--default">
	    <div class="header__inner">
	        <div class="grid-container">
	            <div class="grid-x grid-padding-x">
	                <div class="cell">
	                    <h1 class="h--uppercase">
	                        <?php the_title(); ?>
	                    </h1>
	                </div>
	            </div>
	        </div>
	    </div>
	</header>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
