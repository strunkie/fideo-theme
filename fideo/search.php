<?php /* Template Name: Search template 2 */ 
global $whiteheader;

$whiteheader = true;
get_header(); 
?>

<section class="section bg--white section-search" no-fade>
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <form action="/" class="searchbar">
                    <input type="text" name="s" value="<?php echo htmlspecialchars_decode($_GET['s']); ?>" placeholder="Wat zoek je?">
                    <button class="btn-text">Zoeken</button>
                </form>
                <div class="section-search__content">
                    <?php if(empty($_GET['s'])) : ?>
                        <p>
                            Wat kunnen we voor je doen? Voer je zoekwoord(en) in, zodat we je verder kunnen helpen.
                        </p>
                        <a href="/" class="btn-text">
                            Terug naar home
                        </a>
                    <?php else: ?>
                        <p><strong><?php echo $wp_query->found_posts; ?> resultaten</strong> gevonden voor ‘<?php echo htmlspecialchars_decode($_GET['s']); ?>’</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if($wp_query->found_posts > 0 && !empty($_GET['s'])) : ?>
<section class="section bg--white" no-fade>
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-24 medium-20 large-14">
                <?php get_template_part('search-loop'); ?>

                <?php get_template_part('pagination'); ?>
            </div>
        </div>
    </div>    
</div>
<?php endif; ?>


<?php
get_footer(); ?>