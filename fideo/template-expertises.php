<?php /* Template Name: Expertises template */ get_header(); ?>
	<!-- header -->
	<header class="header header--content">
        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--red">
                            <?php the_title(); ?>
                        </h1>
                        
                        <div class="header__content">
                            <?php if(get_field('header_sub_title')): ?>
							<div class="h5 header__subtitle">
                                <?php echo get_field('header_sub_title'); ?>
                            </div>
                            <?php endif; ?>

                            <?php if(get_field('header_sub')): ?>
							<p class="header__text">
								<?php echo get_field('header_sub'); ?>
							</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(get_field('header_buttontext')) : ?>
            <div class="header__link">
                <a href="#" class="btn-text btn-text--white" scroll-to-content><?php echo get_field('header_buttontext') ?></a>
            </div>
        <?php endif; ?>
    </header>
	<!-- /header -->

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
