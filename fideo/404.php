<?php get_header(); ?>

	<header class="header header--content">
        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--red">
                            Deze pagina konden we niet vinden.
                        </h1>

                        <p>
	    					De pagina die je zoekt bestaat niet of niet meer.
	    				</p>

                        <a href="/" class="btn btn--yellow-red"><span>terug naar home</span> <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

<?php get_footer(); ?>
