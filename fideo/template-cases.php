<?php /* Template Name: Cases template */ get_header(); ?>
	<!-- header -->
	<header class="header header--content">
        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--red">
                            <?php the_title(); ?>
                        </h1>
                        
                        <div class="header__content">
                            <?php if(get_field('header_sub_title')): ?>
							<div class="h5 header__subtitle">
                                <?php echo get_field('header_sub_title'); ?>
                            </div>
                            <?php endif; ?>

                            <?php if(get_field('header_sub')): ?>
							<p class="header__text">
								<?php echo get_field('header_sub'); ?>
							</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(get_field('header_buttontext')) : ?>
            <div class="header__link">
                <a href="#" class="btn-text btn-text--white" scroll-to-content><?php echo get_field('header_buttontext') ?></a>
            </div>
        <?php endif; ?>
    </header>
	<!-- /header -->

	<section class="section bg--white">
	    <div class="grid-container">
	        <div class="grid-x grid-padding-x">
	            <div class="cell">
	                <form action="" method="get" class="filterbar">
	                    <div class="filterbar__item">
	                        <label class="filterbar__label">Bekijk ons werk:</label>
	                        <div class="customselect filterbar__select">
	                            <select name="categorie">
	                                <option value="">Categorie...</option>
	                                <?php
	                                    $categories = get_terms_by_custom_post_type('cases', 'category');
	                    
	                                    foreach($categories as $category) :
	                                        ?>
	                                            <option
	                                                value="<?php echo $category->slug; ?>"
	                                                <?php if(isset($_GET['categorie']) && $_GET['categorie'] == $category->slug) { echo 'selected'; } ?>
	                                            >
	                                                <?php echo $category->name; ?>
	                                            </option>
	                                        <?php
	                                    endforeach;
	                                ?>
	                            </select>
	                        </div>
	                    </div>
	                    <div class="filterbar__item">
							<label class="filterbar__label">voor:</label>
							<div class="customselect filterbar__select">
	                            <select name="doelgroep">
	                                <option value="" selected>Doelgroep...</option>
	                                <?php
	                                    $categories = get_terms_by_custom_post_type('cases', 'doelgroep');
	                    
	                                    foreach($categories as $category) :
	                                        ?>
	                                            <option
	                                                value="<?php echo $category->slug; ?>"
	                                                <?php if(isset($_GET['doelgroep']) && $_GET['doelgroep'] == $category->slug) { echo 'selected'; } ?>
	                                            >
	                                                <?php echo $category->name ?>
	                                            </option>
	                                        <?php
	                                    endforeach;
	                                ?>
	                            </select>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</section>

	<section class="section bg--white">
	    <div class="grid-container">
	        <div class="grid-x grid-padding-x grid-margin-large-y">
	        	<?php
	        		$filters = array();

	        		if(isset($_GET['doelgroep']) && $_GET['doelgroep']) {
	        			$filters[] = array(
		        			'taxonomy' => 'doelgroep',
		        			'field' => 'slug',
		        			'terms' => $_GET['doelgroep']
		        		);
					}

					if(isset($_GET['categorie']) && $_GET['categorie']) {
	        			$filters[] = array(
		        			'taxonomy' => 'category',
		        			'field' => 'slug',
		        			'terms' => $_GET['categorie']
		        		);
					}

	        		$args = array( 'post_type' => 'cases', 'posts_per_page' => -1 );

	        		if(count($filters) > 0) {
	        			$args['tax_query'] = $filters;
	        		}

	        		$loop = new WP_Query( $args );
	        		$i = 1;
	        		while ( $loop->have_posts() ) : $loop->the_post();
	        			global $size;
	        			if($i > 6) {
	        				$size = 'small-24 medium-12 large-8';
	        			} else {
	        				$size = 'small-24 medium-12';
	        			}
	        			
						get_template_part('article-cases');
						$i++;
	        		endwhile;
	        	?>
	        </div>
	    </div>
	</section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
