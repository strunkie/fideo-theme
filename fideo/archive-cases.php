<?php get_header(); ?>

	<section class="section bg--white">
	    <div class="grid-container">
	        <div class="grid-x grid-padding-x small-up-2">
	        	<?php get_template_part('article'); ?>
	        </div>
	    </div>
	</section>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
