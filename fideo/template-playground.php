<?php /* Template Name: Playground Template */ get_header(); ?>
	<!-- header -->
	<header class="header header--home">
        <div class="bg-video" video="<?php echo get_field('header_youtubeid'); ?>">
            <div class="bg-video__cover <?php if(get_field('show_pattern')) { echo 'bg-video__cover--pattern'; } ?>"></div>
            <div id="tv"></div>
        </div>

        <?php
            if( get_field('mobile_vid') && get_field('mobile_vid')['url'] ) :
        ?>
        <div class="bg-video-mobile">
            <video
                id="heroVideoMobile"
                src="<?php echo get_field('mobile_vid')['url'] ?>"
                playsinline
                autoplay
                muted
                loop
            >
            </video>
        </div>

        <?php
            endif;
        ?>

        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--red">
                            <?php the_title(); ?>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- /header -->

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
    <?php endif; ?>

    <section class="section bg--white employees-slider-container">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell">
                    <div class="heading heading--spacing-large">
                        <h2 class="h3 heading__title">Let's team up!</h2>
                        <div class="sliderbuttons">
                            <div class="sliderbuttons__button sliderbuttons__button--prev"><i class="icon-arrow-back"></i></div>
                            <div class="sliderbuttons__button sliderbuttons__button--next"><i class="icon-arrow-forward"></i></div>
                        </div>
                    </div>
                </div>
                <div class="cell">
                    <div class="employees-slider">
                        <div class="employees">
                            <figure class="employees__figure">
                                <img src="//loremimage.com/264/390" alt="">
                            </figure>
                            <div class="employees__undertitle">Camera & regie</div>
                            <h3 class="h2 employees__title">1</h3>
                            <div class="employees__contact">
                                <a href="tel:0612345678">M 06 123 456 78</a><br>
                                <a href="mailto:alexandar@fideo.nl">E alexandar@fideo.nl</a>
                            </div>
                        </div>
                        <div class="employees">
                            <figure class="employees__figure">
                                <img src="//loremimage.com/264/390" alt="">
                            </figure>
                            <div class="employees__undertitle">Camera & regie</div>
                            <h3 class="h2 employees__title">Menno Nauta</h3>
                            <div class="employees__contact">
                                <a href="tel:0612345678">M 06 123 456 78</a><br>
                                <a href="mailto:alexandar@fideo.nl">E alexandar@fideo.nl</a>
                            </div>
                        </div>
                        <div class="employees">
                            <figure class="employees__figure">
                                <img src="//loremimage.com/264/390" alt="">
                            </figure>
                            <div class="employees__undertitle">Camera & regie</div>
                            <h3 class="h2 employees__title">Menno Nauta</h3>
                            <div class="employees__contact">
                                <a href="tel:0612345678">M 06 123 456 78</a><br>
                                <a href="mailto:alexandar@fideo.nl">E alexandar@fideo.nl</a>
                            </div>
                        </div>  
                        <div class="employees">
                            <figure class="employees__figure">
                                <img src="//loremimage.com/264/390" alt="">
                            </figure>
                            <div class="employees__undertitle">Camera & regie</div>
                            <h3 class="h2 employees__title">Menno Nauta</h3>
                            <div class="employees__contact">
                                <a href="tel:0612345678">M 06 123 456 78</a><br>
                                <a href="mailto:alexandar@fideo.nl">E alexandar@fideo.nl</a>
                            </div>
                        </div>
                        <div class="employees">
                            <figure class="employees__figure">
                                <img src="//loremimage.com/264/390" alt="">
                            </figure>
                            <div class="employees__undertitle">Camera & regie</div>
                            <h3 class="h2 employees__title">5</h3>
                            <div class="employees__contact">
                                <a href="tel:0612345678">M 06 123 456 78</a><br>
                                <a href="mailto:alexandar@fideo.nl">E alexandar@fideo.nl</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section bg--white">
        <a href="#" class="btn-text">Ontdek meer</a>
    </section>

    <section class="section bg--light-grey">
    <a href="#" class="btn-text">Ontdek meer</a>
    </section>

    <section class="section bg--red">
    <a href="#" class="btn-text">Ontdek meer</a>
    </section>

    <section class="section bg--white">
    <a href="#" class="btn-text">Ontdek meer</a>
    </section>

    <div class="section">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell medium-offset-1">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad voluptas repellat alias, quas autem aut cumque enim eos nihil ipsa dolores fugiat doloremque voluptates temporibus praesentium laboriosam quo suscipit architecto ratione est, nulla sunt, provident accusamus pariatur? Corporis quas esse facilis dolores eaque numquam, possimus sequi provident quasi minima commodi perspiciatis tenetur libero labore voluptatum repellendus cum. Cumque dolores, nostrum illo eum distinctio aperiam recusandae illum veritatis ad, similique cupiditate harum porro atque excepturi labore deserunt id unde, odio vero est laborum quod animi accusantium. Atque velit qui culpa, sequi eum asperiores labore, vero facere voluptates eligendi accusamus, adipisci natus?</p>
                    <ul>
                        <li>test</li>
                        <li>test</li>
                        <li>test</li>
                        <li>test</li>
                    </ul>
                    <ol>
                        <li>test</li>
                        <li>test</li>
                        <li>test</li>
                        <li>test</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
        
    <section class="section bg--light-grey">
        <div class="grid-container">
            <div class="grid-x grid-padding-x align-right">
                <div class="cell small-24 medium-15 large-14">

                    <h2 class="h3 heading heading--spacing-large">Solliciteer direct</h2>

                    <form class="form form--bg-light-grey form--spacing-large ajaxform" method="post" action="/_tmp/form-ajax.php" id="form1" novalidate ajaxform>

                        <div class="grid-x grid-padding-x">
                            <div class="cell small-24 medium-12 large-12">
                                <div class="formfield formfield--input">
                                    <label class="formfield__label">Mijn voornaam is</label>
                                
                                    <ul class="form__errors" data-errorgroup="node_text"> 
                                    </ul>
                                
                                    <input type="text" placeholder="voornaam..." name="node_email" fieldID="node_text">
                                </div>
                            </div>
                            <div class="cell small-24 medium-12 large-12">
                                <div class="formfield formfield--input">
                                    <label class="formfield__label">Mijn achternaam is</label>
                                
                                    <ul class="form__errors" data-errorgroup="node_text"> 
                                    </ul>
                                
                                    <input type="text" placeholder="achternaam..." name="node_email" fieldID="node_text">
                                </div>
                            </div>
                        </div>

                        <div class="grid-x grid-padding-x">
                            <div class="cell small-24 medium-12 large-12">
                                <div class="formfield formfield--input">
                                    <label class="formfield__label">Mijn emailadres is</label>
                                
                                    <ul class="form__errors" data-errorgroup="node_text"> 
                                    </ul>
                                
                                    <input type="text" placeholder="emailadres..." name="node_email" fieldID="node_text">
                                </div>
                            </div>
                            <div class="cell small-24 medium-12 large-12">
                                <div class="formfield formfield--input">
                                    <label class="formfield__label">Mijn telefoonnummer is</label>
                                
                                    <ul class="form__errors" data-errorgroup="node_text"> 
                                    </ul>
                                
                                    <input type="text" placeholder="telefoonnummer..." name="node_email" fieldID="node_text">
                                </div>
                            </div>
                        </div>

                        <div class="grid-x grid-padding-x">
                            <div class="cell small-24">
                                <div class="formfield formfield--textarea">
                                    <label class="formfield__label">Mijn motivatie</label>

                                    <ul class="form__errors" data-errorgroup="node_text"> 
                                    </ul>
                                
                                    <textarea placeholder="type een korte motivatie"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="grid-x grid-padding-x">
                            <div class="cell small-24">
                                <div class="formfield formfield--inputlist">
                                    <ul class="form__errors" data-errorgroup="node_text"> 
                                    </ul>
                                
                                    <ul>
                                        <li>
                                            <label class="formfield__choice">
                                                <input type="checkbox" name="checkbox" fieldID="node_text">
                                                <span>Ja, ik wil op de hoogte blijven van de laatste trends en ontvang graag marketingtips via email</span>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="formfield__choice">
                                                <input type="checkbox" name="checkbox" fieldID="node_text">
                                                <span>Ik ga akkoord met de voorwaarden</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="formfield formfield--button">
                            <button>
                                <div class="btn">
                                    <span>Solliciteer</span>
                                    <i class="icon-arrow-right"></i>
                                </div>
                            </button>
                    
                            <span class="loader"></span>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
    </section>

    <div class="notificationbar">
        <div class="notificationbar__main">
            <p>Fideo heeft een nieuwe brand identity. </p>
            <a href="#" class="btn-text btn-text--white">Ontdek meer</a>
        </div>
        <button class="notificationbar__icon"><i class="icon-close"></i></button>
    </div>

    <section class="section bg--white slider-container">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell">
                    <div class="slider">
                        <div class="slider__slides">
                            <div class="slider__slide slider__slide--grey-panel">
                                <div class="step">
                                    <div class="step__inner">
                                        <div>
                                            <div class="step__number">1</div>
                                            <div class="step__content">
                                                <h3 class="h2"><span class="step__number-small">1 </span>Element werkwijze stappen</h3>
                                                <p>
                                                    Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean lacinia bibendum nulla sed consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__slide slider__slide--grey-panel">
                                <div class="step">
                                    <div class="step__inner">
                                        <div>
                                            <div class="step__number">1</div>
                                            <div class="step__content">
                                                <h3 class="h2"><span class="step__number-small">1 </span>Element werkwijze stappen</h3>
                                                <p>
                                                    Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean lacinia bibendum nulla sed consectetur. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="slider__buttons">
                            <div class="slider__button slider__button--prev"><i class="icon-arrow-back"></i></div>
                            <div class="slider__button slider__button--next"><i class="icon-arrow-forward"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section bg--white">
        <div class="grid-container">
            <div class="grid-x grid-padding-x">
                <div class="cell small-24 medium-8 large-6"></div>
                <div class="cell small-24 medium-offset-1 medium-15 large-offset-2 large-14">
                    <div class="grid-x grid-margin-y article-listview">
                        <?php for($x = 0; $x < 4; $x++) {?>
                        <a href="#" class="cell article-listitem article">
                            <div class="grid-x grid-padding-x align-middle">
                                <div class="cell small-6">
                                    <img src="//loremimage.com/400/400" alt="">
                                </div>
                                <div class="cell small-18">
                                    <div class="article-listitem__undertitle">strategie</div>
                                    <h3 class="h2 article__title">Amed laris dalaros dit adori Fideo danos</h2>
                                </div>
                            </div>
                        </a>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
