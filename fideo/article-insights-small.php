<a class="cell article-listitem article" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" <?php if($video) { ?>hover-video<?php } ?>>
    <div class="grid-x grid-padding-x align-middle">
        <div class="cell small-6">
            <img class="article__figure__image" src="<?php echo get_field('highlight_afbeelding_static')['sizes']['overview-image'] ?>" alt="<?php the_title(); ?>">
        </div>
        <div class="cell small-18">
            <?php if(get_field('overview_subtitle')) : ?>
                <div class="article-listitem__undertitle"><?php echo get_field('overview_subtitle'); ?></div>
            <?php endif; ?>
            
            <h3 class="h2 article__title"><?php echo get_field('alternative_title') ?: the_title(); ?></h3>
        </div>
    </div>
</a>