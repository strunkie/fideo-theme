<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PL2JFSN');</script>
        <!-- End Google Tag Manager -->

	</head>
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PL2JFSN"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

		<!-- wrapper -->
		<div class="wrapper">
			<?php
			global $whiteheader;
			?>

            <div class="topbar <?php if($whiteheader) { echo 'topbar--white'; } ?>">                
                <div class="topbar__inner">
                    <div class="grid-container">
                        <div class="grid-x div grid-padding-x align-middle">
                            <div class="cell auto">
                                <div class="topbar__logo">
                                    <a href="/">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logos/logo-fideo.svg" width="125px" alt="">
                                    </a>

                                    <div class="payoff">
                                        <div class="payoff__top">Let's</div>
                                        <div class="payoff__bottom">
                                            &nbsp;
                                            <?php
                                                $items = explode("\n", get_option( 'fideo_option_name' )['slogans_0']);
                                                foreach($items as $item) :
                                            ?>
                                                <div><?php echo $item ?></div>
                                            <?php
                                                endforeach;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cell shrink">
                                <div class="topbar__right">
                                    <a class="topbar__search" href="/?s">
                                        <i class="icon-search"></i>
                                    </a>
                                    <div class="topbar__menu">
                                        <a class="hamburger" href="#">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </a>
                                        <div class="circle-overlay-yellow"></div>
                                        <div class="circle-overlay-blue"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>

			<div class="mainnav" style="display: none">
				<div class="mainnav__inner">
					<div class="grid-container">
						<div class="grid-y grid-padding-x">
							<div class="cell auto">
								<ul class="mainnav__items mainnav__items--main">
									<?php foreach(wp_get_nav_menu_items('Hoofdmenu') as $item) : ?>
										<li><a href="<?php echo $item->url; ?>"><?php echo $item->title ?></a></li>
									<?php endforeach; ?>
								</ul>
								
								<ul class="mainnav__items mainnav__items--sub">
									<?php foreach(wp_get_nav_menu_items('HoofdmenuSub') as $item) : ?>
										<li><a href="<?php echo $item->url; ?>"><?php echo $item->title ?></a></li>
									<?php endforeach; ?>
								</ul>
							</div>

							<div class="cell shrink">
								<div class="mainnav__contact">
									<div>Schuttevaerkade 80, 8021 DB Zwolle</div>

									<div class="icon-text">
										<i class="icon-email icon-text--icon-top"></i>
										<a href="mailto:info@fideo.nl" class="btn-text">info@fideo.nl</a>
									</div>

									<div class="icon-text">
										<i class="icon-phone"></i>
										<a href="tel:038 458 10 75" class="btn-text">038 458 10 75</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
            
            <?php
                if(get_option( 'fideo_option_name' )['notificatie_balk_tonen_1']) :
            ?>
            <div class="notificationbar">
                <div class="notificationbar__main">
                    <p><?php echo get_option('fideo_option_name')['notificatie_balk_tekst_2']; ?></p>
                    <a href="<?php echo get_option('fideo_option_name')['notificatie_balk_knop_tekst_4'] ?>" class="btn-text btn-text--white"><?php echo get_option('fideo_option_name')['notificatie_balk_link_3']; ?></a>
                </div>
                <button class="notificationbar__icon"><i class="icon-close"></i></button>
            </div>

            <?php 
                endif;
            ?>