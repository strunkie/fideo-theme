<?php /* Template Name: Insights template */ get_header(); ?>
	<!-- header -->
	<header class="header header--content">
        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--red">
                            <?php the_title(); ?>
                        </h1>
                        
                        <div class="header__content">
                            <?php if(get_field('header_sub_title')): ?>
							<div class="h5 header__subtitle">
                                <?php echo get_field('header_sub_title'); ?>
                            </div>
                            <?php endif; ?>

                            <?php if(get_field('header_sub')): ?>
							<p class="header__text">
								<?php echo get_field('header_sub'); ?>
							</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(get_field('header_buttontext')) : ?>
            <div class="header__link">
                <a href="#" class="btn-text btn-text--white" scroll-to-content><?php echo get_field('header_buttontext') ?></a>
            </div>
        <?php endif; ?>
    </header>
	<!-- /header -->

	<section class="section bg--white">
	    <div class="grid-container">
	        <div class="grid-x grid-padding-x">
	            <div class="cell">
	                <form action="" method="get" class="filterbar">
	                    <div class="filterbar__item">
	                        <label class="filterbar__label">Waar wil je iets over weten?</label>
	                        <div class="customselect filterbar__select">
	                            <select name="categorie">
	                                <option value="">Categorie...</option>
	                                <?php
	                                    $categories = get_terms_by_custom_post_type('insights', 'category');
	                            
	                                    foreach($categories as $category) :
	                                        ?>
	                                            <option
	                                                value="<?php echo $category->slug; ?>"
	                                                <?php if(isset($_GET['categorie']) && $_GET['categorie'] == $category->slug) { echo 'selected'; } ?>
	                                            >
	                                                <?php echo $category->name; ?>
	                                            </option>
	                                        <?php
	                                    endforeach;
	                                ?>
	                            </select>
	                        </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</section>

	<section class="section bg--white">
	    <div class="grid-container">
	        <div class="grid-x grid-padding-x grid-margin-large-y small-up-1 medium-up-2 large-up-3">
	        	<?php
	        		$filters = array();

	        		if(isset($_GET['doelgroep']) && $_GET['doelgroep']) {
	        			$filters[] = array(
		        			'taxonomy' => 'doelgroep',
		        			'field' => 'slug',
		        			'terms' => $_GET['doelgroep']
		        		);
					}

					if(isset($_GET['categorie']) && $_GET['categorie']) {
	        			$filters[] = array(
		        			'taxonomy' => 'category',
		        			'field' => 'slug',
		        			'terms' => $_GET['categorie']
		        		);
					}

	        		$args = array( 'post_type' => 'insights', 'posts_per_page' => 12, 'offset' => 0 );

	        		if(count($filters) > 0) {
	        			$args['tax_query'] = $filters;
	        		}

	        		$loop = new WP_Query( $args );
	        		while ( $loop->have_posts() ) : $loop->the_post();
	        		  	get_template_part('article-insights');
	        		endwhile;
	        	?>
	        </div>
	    </div>
	</section>

	<?php
		$args = array( 'post_type' => 'insights', 'posts_per_page' => 9999999999, 'offset' => 12 );

		if(count($filters) > 0) {
			$args['tax_query'] = $filters;
		}

		$loop = new WP_Query( $args );
		if ($loop->have_posts()) {
			?>
				<section class="section bg--white">
				    <div class="grid-container">
				        <div class="grid-x grid-padding-x">
				            <div class="cell small-24 medium-24 large-6"></div>
				            <div class="cell small-24 medium-24 large-offset-2 large-14">
				                <div class="grid-x grid-margin-y article-listview">
			<?php
			while ( $loop->have_posts() ) : $loop->the_post();
			  get_template_part('article-insights-small');
			endwhile;
			?>
								</div>
							</div>
						</div>
					</div>
				</section>
			<?php
		}
	?>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
