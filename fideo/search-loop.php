<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<a href="<?php the_permalink(); ?>" class="search-item">
        <!-- <div class="text-grey"><?php echo get_post_type(); ?></div> -->
        <h3 class="h2"><?php the_title(); ?></h3>
        <?php if(!empty(get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true))) : ?>
        <p>
            <?php echo get_post_meta(get_the_ID(), '_yoast_wpseo_metadesc', true);  ?>
        </p>
    	<?php endif; ?>

        <span class="btn-text">lees meer</span>
    </a>

<?php endwhile; ?>

<?php endif; ?>
