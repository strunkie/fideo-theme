<?php
function partners() {

    $labels = array(
        'name'                  => _x( 'partners', 'Post Type General Name', 'partners' ),
        'singular_name'         => _x( 'partner', 'Post Type Singular Name', 'partners' ),
        'menu_name'             => __( 'partners', 'partners' ),
        'name_admin_bar'        => __( 'partners', 'partners' ),
        'archives'              => __( 'Item Archives', 'partners' ),
        'attributes'            => __( 'Item Attributes', 'partners' ),
        'parent_item_colon'     => __( 'Parent Item:', 'partners' ),
        'all_items'             => __( 'All Items', 'partners' ),
        'add_new_item'          => __( 'Add New Item', 'partners' ),
        'add_new'               => __( 'Add New', 'partners' ),
        'new_item'              => __( 'New Item', 'partners' ),
        'edit_item'             => __( 'Edit Item', 'partners' ),
        'update_item'           => __( 'Update Item', 'partners' ),
        'view_item'             => __( 'View Item', 'partners' ),
        'view_items'            => __( 'View Items', 'partners' ),
        'search_items'          => __( 'Search Item', 'partners' ),
        'not_found'             => __( 'Not found', 'partners' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'partners' ),
        'featured_image'        => __( 'Featured Image', 'partners' ),
        'set_featured_image'    => __( 'Set featured image', 'partners' ),
        'remove_featured_image' => __( 'Remove featured image', 'partners' ),
        'use_featured_image'    => __( 'Use as featured image', 'partners' ),
        'insert_into_item'      => __( 'Insert into item', 'partners' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'partners' ),
        'items_list'            => __( 'Items list', 'partners' ),
        'items_list_navigation' => __( 'Items list navigation', 'partners' ),
        'filter_items_list'     => __( 'Filter items list', 'partners' ),
    );
    $args = array(
        'label'                 => __( 'partner', 'partners' ),
        'description'           => __( 'partners', 'partners' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => false,
        'rewrite'               => false,
        'query_var'             => false,
        'publicly_queryable'    => false
    );
    register_post_type( 'partners', $args );

}
add_action( 'init', 'partners', 0 );