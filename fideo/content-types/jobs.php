<?php
function jobs() {

    $labels = array(
        'name'                  => _x( 'jobs', 'Post Type General Name', 'jobs' ),
        'singular_name'         => _x( 'job', 'Post Type Singular Name', 'jobs' ),
        'menu_name'             => __( 'jobs', 'jobs' ),
        'name_admin_bar'        => __( 'jobs', 'jobs' ),
        'archives'              => __( 'Item Archives', 'jobs' ),
        'attributes'            => __( 'Item Attributes', 'jobs' ),
        'parent_item_colon'     => __( 'Parent Item:', 'jobs' ),
        'all_items'             => __( 'All Items', 'jobs' ),
        'add_new_item'          => __( 'Add New Item', 'jobs' ),
        'add_new'               => __( 'Add New', 'jobs' ),
        'new_item'              => __( 'New Item', 'jobs' ),
        'edit_item'             => __( 'Edit Item', 'jobs' ),
        'update_item'           => __( 'Update Item', 'jobs' ),
        'view_item'             => __( 'View Item', 'jobs' ),
        'view_items'            => __( 'View Items', 'jobs' ),
        'search_items'          => __( 'Search Item', 'jobs' ),
        'not_found'             => __( 'Not found', 'jobs' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'jobs' ),
        'featured_image'        => __( 'Featured Image', 'jobs' ),
        'set_featured_image'    => __( 'Set featured image', 'jobs' ),
        'remove_featured_image' => __( 'Remove featured image', 'jobs' ),
        'use_featured_image'    => __( 'Use as featured image', 'jobs' ),
        'insert_into_item'      => __( 'Insert into item', 'jobs' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'jobs' ),
        'items_list'            => __( 'Items list', 'jobs' ),
        'items_list_navigation' => __( 'Items list navigation', 'jobs' ),
        'filter_items_list'     => __( 'Filter items list', 'jobs' ),
    );
    $args = array(
        'label'                 => __( 'job', 'jobs' ),
        'description'           => __( 'jobs', 'jobs' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'taxonomies'            => array( 'category' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'jobs', $args );

}
add_action( 'init', 'jobs', 0 );