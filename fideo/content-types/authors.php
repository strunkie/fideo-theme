<?php
function authors() {

    $labels = array(
        'name'                  => _x( 'authors', 'Post Type General Name', 'authors' ),
        'singular_name'         => _x( 'author', 'Post Type Singular Name', 'authors' ),
        'menu_name'             => __( 'authors', 'authors' ),
        'name_admin_bar'        => __( 'authors', 'authors' ),
        'archives'              => __( 'Item Archives', 'authors' ),
        'attributes'            => __( 'Item Attributes', 'authors' ),
        'parent_item_colon'     => __( 'Parent Item:', 'authors' ),
        'all_items'             => __( 'All Items', 'authors' ),
        'add_new_item'          => __( 'Add New Item', 'authors' ),
        'add_new'               => __( 'Add New', 'authors' ),
        'new_item'              => __( 'New Item', 'authors' ),
        'edit_item'             => __( 'Edit Item', 'authors' ),
        'update_item'           => __( 'Update Item', 'authors' ),
        'view_item'             => __( 'View Item', 'authors' ),
        'view_items'            => __( 'View Items', 'authors' ),
        'search_items'          => __( 'Search Item', 'authors' ),
        'not_found'             => __( 'Not found', 'authors' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'authors' ),
        'featured_image'        => __( 'Featured Image', 'authors' ),
        'set_featured_image'    => __( 'Set featured image', 'authors' ),
        'remove_featured_image' => __( 'Remove featured image', 'authors' ),
        'use_featured_image'    => __( 'Use as featured image', 'authors' ),
        'insert_into_item'      => __( 'Insert into item', 'authors' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'authors' ),
        'items_list'            => __( 'Items list', 'authors' ),
        'items_list_navigation' => __( 'Items list navigation', 'authors' ),
        'filter_items_list'     => __( 'Filter items list', 'authors' ),
    );
    $args = array(
        'label'                 => __( 'author', 'authors' ),
        'description'           => __( 'authors', 'authors' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => false,
        'rewrite'               => false,
        'query_var'             => false,
        'publicly_queryable'    => false
    );
    register_post_type( 'authors', $args );

}
add_action( 'init', 'authors', 0 );