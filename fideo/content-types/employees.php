<?php
function employees() {

    $labels = array(
        'name'                  => _x( 'employees', 'Post Type General Name', 'employees' ),
        'singular_name'         => _x( 'employee', 'Post Type Singular Name', 'employees' ),
        'menu_name'             => __( 'employees', 'employees' ),
        'name_admin_bar'        => __( 'employees', 'employees' ),
        'archives'              => __( 'Item Archives', 'employees' ),
        'attributes'            => __( 'Item Attributes', 'employees' ),
        'parent_item_colon'     => __( 'Parent Item:', 'employees' ),
        'all_items'             => __( 'All Items', 'employees' ),
        'add_new_item'          => __( 'Add New Item', 'employees' ),
        'add_new'               => __( 'Add New', 'employees' ),
        'new_item'              => __( 'New Item', 'employees' ),
        'edit_item'             => __( 'Edit Item', 'employees' ),
        'update_item'           => __( 'Update Item', 'employees' ),
        'view_item'             => __( 'View Item', 'employees' ),
        'view_items'            => __( 'View Items', 'employees' ),
        'search_items'          => __( 'Search Item', 'employees' ),
        'not_found'             => __( 'Not found', 'employees' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'employees' ),
        'featured_image'        => __( 'Featured Image', 'employees' ),
        'set_featured_image'    => __( 'Set featured image', 'employees' ),
        'remove_featured_image' => __( 'Remove featured image', 'employees' ),
        'use_featured_image'    => __( 'Use as featured image', 'employees' ),
        'insert_into_item'      => __( 'Insert into item', 'employees' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'employees' ),
        'items_list'            => __( 'Items list', 'employees' ),
        'items_list_navigation' => __( 'Items list navigation', 'employees' ),
        'filter_items_list'     => __( 'Filter items list', 'employees' ),
    );
    $args = array(
        'label'                 => __( 'employee', 'employees' ),
        'description'           => __( 'employees', 'employees' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => false,
        'rewrite'               => false,
        'query_var'             => false,
        'publicly_queryable'    => false
    );
    register_post_type( 'employees', $args );

}
add_action( 'init', 'employees', 0 );