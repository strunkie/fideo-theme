<?php
function insights() {

    $labels = array(
        'name'                  => _x( 'insights', 'Post Type General Name', 'insights' ),
        'singular_name'         => _x( 'Insight', 'Post Type Singular Name', 'insight' ),
        'menu_name'             => __( 'Blogs', 'insights' ),
        'name_admin_bar'        => __( 'Blogs', 'insights' ),
        'archives'              => __( 'Item Archives', 'insights' ),
        'attributes'            => __( 'Item Attributes', 'insights' ),
        'parent_item_colon'     => __( 'Parent Item:', 'insights' ),
        'all_items'             => __( 'All Items', 'insights' ),
        'add_new_item'          => __( 'Add New Item', 'insights' ),
        'add_new'               => __( 'Add New', 'insights' ),
        'new_item'              => __( 'New Item', 'insights' ),
        'edit_item'             => __( 'Edit Item', 'insights' ),
        'update_item'           => __( 'Update Item', 'insights' ),
        'view_item'             => __( 'View Item', 'insights' ),
        'view_items'            => __( 'View Items', 'insights' ),
        'search_items'          => __( 'Search Item', 'insights' ),
        'not_found'             => __( 'Not found', 'insights' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'insights' ),
        'featured_image'        => __( 'Featured Image', 'insights' ),
        'set_featured_image'    => __( 'Set featured image', 'insights' ),
        'remove_featured_image' => __( 'Remove featured image', 'insights' ),
        'use_featured_image'    => __( 'Use as featured image', 'insights' ),
        'insert_into_item'      => __( 'Insert into item', 'insights' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'insights' ),
        'items_list'            => __( 'Items list', 'insights' ),
        'items_list_navigation' => __( 'Items list navigation', 'insights' ),
        'filter_items_list'     => __( 'Filter items list', 'insights' ),
    );
    $args = array(
        'label'                 => __( 'Insight', 'insights' ),
        'description'           => __( 'insights', 'insights' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'taxonomies'            => array( 'category' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'insights', $args );

}
add_action( 'init', 'insights', 0 );