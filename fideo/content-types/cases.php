<?php
function cases() {

    $labels = array(
        'name'                  => _x( 'Cases', 'Post Type General Name', 'cases' ),
        'singular_name'         => _x( 'Case', 'Post Type Singular Name', 'case' ),
        'menu_name'             => __( 'Cases', 'cases' ),
        'name_admin_bar'        => __( 'Cases', 'cases' ),
        'archives'              => __( 'Item Archives', 'cases' ),
        'attributes'            => __( 'Item Attributes', 'cases' ),
        'parent_item_colon'     => __( 'Parent Item:', 'cases' ),
        'all_items'             => __( 'All Items', 'cases' ),
        'add_new_item'          => __( 'Add New Item', 'cases' ),
        'add_new'               => __( 'Add New', 'cases' ),
        'new_item'              => __( 'New Item', 'cases' ),
        'edit_item'             => __( 'Edit Item', 'cases' ),
        'update_item'           => __( 'Update Item', 'cases' ),
        'view_item'             => __( 'View Item', 'cases' ),
        'view_items'            => __( 'View Items', 'cases' ),
        'search_items'          => __( 'Search Item', 'cases' ),
        'not_found'             => __( 'Not found', 'cases' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'cases' ),
        'featured_image'        => __( 'Featured Image', 'cases' ),
        'set_featured_image'    => __( 'Set featured image', 'cases' ),
        'remove_featured_image' => __( 'Remove featured image', 'cases' ),
        'use_featured_image'    => __( 'Use as featured image', 'cases' ),
        'insert_into_item'      => __( 'Insert into item', 'cases' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'cases' ),
        'items_list'            => __( 'Items list', 'cases' ),
        'items_list_navigation' => __( 'Items list navigation', 'cases' ),
        'filter_items_list'     => __( 'Filter items list', 'cases' ),
    );
    $args = array(
        'label'                 => __( 'Case', 'cases' ),
        'description'           => __( 'Cases', 'cases' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'taxonomies'            => array( 'category', 'post_tag', 'doelgroep' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'cases', $args );

}
add_action( 'init', 'cases', 0 );