<?php
function expertises() {

    $labels = array(
        'name'                  => _x( 'expertises', 'Post Type General Name', 'expertises' ),
        'singular_name'         => _x( 'expertise', 'Post Type Singular Name', 'expertises' ),
        'menu_name'             => __( 'expertises', 'expertises' ),
        'name_admin_bar'        => __( 'expertises', 'expertises' ),
        'archives'              => __( 'Item Archives', 'expertises' ),
        'attributes'            => __( 'Item Attributes', 'expertises' ),
        'parent_item_colon'     => __( 'Parent Item:', 'expertises' ),
        'all_items'             => __( 'All Items', 'expertises' ),
        'add_new_item'          => __( 'Add New Item', 'expertises' ),
        'add_new'               => __( 'Add New', 'expertises' ),
        'new_item'              => __( 'New Item', 'expertises' ),
        'edit_item'             => __( 'Edit Item', 'expertises' ),
        'update_item'           => __( 'Update Item', 'expertises' ),
        'view_item'             => __( 'View Item', 'expertises' ),
        'view_items'            => __( 'View Items', 'expertises' ),
        'search_items'          => __( 'Search Item', 'expertises' ),
        'not_found'             => __( 'Not found', 'expertises' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'expertises' ),
        'featured_image'        => __( 'Featured Image', 'expertises' ),
        'set_featured_image'    => __( 'Set featured image', 'expertises' ),
        'remove_featured_image' => __( 'Remove featured image', 'expertises' ),
        'use_featured_image'    => __( 'Use as featured image', 'expertises' ),
        'insert_into_item'      => __( 'Insert into item', 'expertises' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'expertises' ),
        'items_list'            => __( 'Items list', 'expertises' ),
        'items_list_navigation' => __( 'Items list navigation', 'expertises' ),
        'filter_items_list'     => __( 'Filter items list', 'expertises' ),
    );
    $args = array(
        'label'                 => __( 'expertise', 'expertises' ),
        'description'           => __( 'expertises', 'expertises' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'taxonomies'            => array( 'category' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'expertises', $args );

}
add_action( 'init', 'expertises', 0 );