<?php

// error_reporting(E_ALL);
// ini_set('display_errors', TRUE);
// ini_set('display_startup_errors', TRUE);
/*
 *  Author: Hacomedia
 *  URL: hacomedia.nl
 *  Fideo
 */

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

global $nofooter;
global $whiteheader;
global $manifestdata;

$manifestdata = json_decode(file_get_contents(__DIR__  . '/assets/manifest.json'), true);

if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support

    add_image_size( 'overview-image', 720, 480, true );
    add_image_size( 'slider', 1200, 700, true );
    add_image_size( 'partner', 300, 300, false );
    add_image_size( 'author', 100, 100, true );
    add_image_size( 'widget', 600, 600, true );
    add_image_size( 'employee', 530, 780, true );
	add_image_size( 'header', 1920, 1080, true );
	
	add_filter( 'image_size_names_choose', 'child_custom_sizes' );

    function child_custom_sizes( $sizes ) {

        return array_merge( $sizes, array(
            'overview-image' => 'overview-image',
			'slider' => 'slider',
			'partner' => 'partner',
			'author' => 'author',
			'widget' => 'widget',
			'employee' => 'employee',
			'header' => 'header'
        ) );
    }

    // Add Support for Custom Backgrounds - Uncomment below if you're going to use
    /*add_theme_support('custom-background', array(
	'default-color' => 'FFF',
	'default-image' => get_template_directory_uri() . '/img/bg.jpg'
    ));*/

    // Add Support for Custom Header - Uncomment below if you're going to use
    /*add_theme_support('custom-header', array(
	'default-image'			=> get_template_directory_uri() . '/img/headers/default.jpg',
	'header-text'			=> false,
	'default-text-color'		=> '000',
	'width'				=> 1000,
	'height'			=> 198,
	'random-default'		=> false,
	'wp-head-callback'		=> $wphead_cb,
	'admin-head-callback'		=> $adminhead_cb,
	'admin-preview-callback'	=> $adminpreview_cb
    ));*/

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// HTML5 Blank navigation
function html5blank_nav()
{
	wp_nav_menu(
	array(
		'theme_location'  => 'header-menu',
		'menu'            => '',
		'container'       => 'div',
		'container_class' => 'menu-{menu slug}-container',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'echo'            => true,
		'fallback_cb'     => 'wp_page_menu',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '<ul>%3$s</ul>',
		'depth'           => 0,
		'walker'          => ''
		)
	);
}


// Load HTML5 Blank conditional scripts
function html5blank_conditional_scripts()
{
    global $manifestdata;
    wp_register_script('vendor', $manifestdata['vendors.js'], null, '1.0.0', true);
    wp_enqueue_script('vendor'); // Enqueue it!

    wp_register_script('scriptname', $manifestdata['main.js'], null, '1.0.0', true);
    wp_enqueue_script('scriptname'); // Enqueue it!
}

// Load HTML5 Blank styles
function html5blank_styles()
{
    global $manifestdata;
    wp_register_style('html5blank', $manifestdata['main.css'], array(), '1.0', 'all');
    wp_enqueue_style('html5blank'); // Enqueue it!
}

// Register HTML5 Blank Navigation
function register_html5_menu()
{
    register_nav_menus(array( // Using array to specify more menus if needed
        'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
        'sidebar-menu' => __('Sidebar Menu', 'html5blank'), // Sidebar Navigation
        'extra-menu' => __('Extra Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
    ));
}

// Remove the <div> surrounding the dynamic navigation to cleanup markup
function my_wp_nav_menu_args($args = '')
{
    $args['container'] = false;
    return $args;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}

// Custom View Article link to Post
function html5_blank_view_article($more)
{
    global $post;
    return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'html5blank') . '</a>';
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function html5blankgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}

// Custom Comments Callback
function html5blankcomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

/*------------------------------------*\
	Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('wp_print_scripts', 'html5blank_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'html5blank_styles'); // Add Theme Stylesheet
add_action('init', 'register_html5_menu'); // Add HTML5 Blank Menu
add_action('init', 'create_post_type_html5'); // Add our HTML5 Blank Custom Post Type
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'html5blankgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
// add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

// Shortcodes
add_shortcode('html5_shortcode_demo', 'html5_shortcode_demo'); // You can place [html5_shortcode_demo] in Pages, Posts now.
add_shortcode('html5_shortcode_demo_2', 'html5_shortcode_demo_2'); // Place [html5_shortcode_demo_2] in Pages, Posts now.

// Shortcodes above would be nested like this -
// [html5_shortcode_demo] [html5_shortcode_demo_2] Here's the page title! [/html5_shortcode_demo_2] [/html5_shortcode_demo]

/*------------------------------------*\
	Custom Post Types
\*------------------------------------*/

// Create 1 Custom Post type for a Demo, called HTML5-Blank
function create_post_type_html5()
{
    register_taxonomy_for_object_type('category', 'html5-blank'); // Register Taxonomies for Category
    register_taxonomy_for_object_type('post_tag', 'html5-blank');
    register_post_type('html5-blank', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('HTML5 Blank Custom Post', 'html5blank'), // Rename these to suit
            'singular_name' => __('HTML5 Blank Custom Post', 'html5blank'),
            'add_new' => __('Add New', 'html5blank'),
            'add_new_item' => __('Add New HTML5 Blank Custom Post', 'html5blank'),
            'edit' => __('Edit', 'html5blank'),
            'edit_item' => __('Edit HTML5 Blank Custom Post', 'html5blank'),
            'new_item' => __('New HTML5 Blank Custom Post', 'html5blank'),
            'view' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'view_item' => __('View HTML5 Blank Custom Post', 'html5blank'),
            'search_items' => __('Search HTML5 Blank Custom Post', 'html5blank'),
            'not_found' => __('No HTML5 Blank Custom Posts found', 'html5blank'),
            'not_found_in_trash' => __('No HTML5 Blank Custom Posts found in Trash', 'html5blank')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'thumbnail'
        ), // Go to Dashboard Custom HTML5 Blank post for supports
        'can_export' => true, // Allows export in Tools > Export
        'taxonomies' => array(
            'post_tag',
            'category'
        ) // Add Category and Post Tags support
    ));
}

/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/

// Shortcode Demo with Nested Capability
function html5_shortcode_demo($atts, $content = null)
{
    return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
}

// Shortcode Demo with simple <h2> tag
function html5_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
{
    return '<h2>' . $content . '</h2>';
}

// Register Custom Taxonomy
function doelgroep_taxonomy() {

    $labels = array(
        'name'                       => _x( 'doelgroepen', 'doelgroepen', 'text_domain' ),
        'singular_name'              => _x( 'doelgroep', 'Doelgroep', 'text_domain' ),
        'menu_name'                  => __( 'Doelgroepen', 'text_domain' ),
        'all_items'                  => __( 'All Items', 'text_domain' ),
        'parent_item'                => __( 'Parent Item', 'text_domain' ),
        'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
        'new_item_name'              => __( 'New Item Name', 'text_domain' ),
        'add_new_item'               => __( 'Add New Item', 'text_domain' ),
        'edit_item'                  => __( 'Edit Item', 'text_domain' ),
        'update_item'                => __( 'Update Item', 'text_domain' ),
        'view_item'                  => __( 'View Item', 'text_domain' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
        'popular_items'              => __( 'Popular Items', 'text_domain' ),
        'search_items'               => __( 'Search Items', 'text_domain' ),
        'not_found'                  => __( 'Not Found', 'text_domain' ),
        'no_terms'                   => __( 'No items', 'text_domain' ),
        'items_list'                 => __( 'Items list', 'text_domain' ),
        'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'show_in_rest'               => true,
    );
    register_taxonomy( 'doelgroep', array('cases'), $args );

}
add_action( 'init', 'doelgroep_taxonomy', 0 );

include('content-types/cases.php');
include('content-types/insights.php');
include('content-types/expertises.php');
include('content-types/partners.php');
include('content-types/jobs.php');
include('content-types/authors.php');
include('content-types/employees.php');


flush_rewrite_rules( false );

include('blocks/related/related.php');
include('blocks/overview/overview.php');
include('blocks/slider/slider.php');
include('blocks/usps/usps.php');
include('blocks/linklist/linklist.php');
include('blocks/logos/logos.php');
include('blocks/widgetvideo/widgetvideo.php');
include('blocks/widgetquestion/widgetquestion.php');
include('blocks/share/share.php');
include('blocks/contact-cta/contact-cta.php');
include('blocks/employees/employees.php');
include('blocks/intro/intro.php');
include('blocks/jobapply/jobapply.php');
include('blocks/vimeo/vimeo.php');
include('blocks/youtube/youtube.php');
include('blocks/circle/circle.php');
include('blocks/steps/steps.php');

function get_terms_by_custom_post_type( $post_type, $taxonomy ){
    $args = array( 'post_type' => $post_type);
    $loop = new WP_Query( $args );
    $postids = array();
    // build an array of post IDs
    while ( $loop->have_posts() ) : $loop->the_post();
    array_push($postids, get_the_ID());
    endwhile;
    // get taxonomy values based on array of IDs
    $regions = wp_get_object_terms( $postids,  $taxonomy );
    return $regions;
}

add_filter( 'allowed_block_types', 'misha_allowed_block_types' );
 
function misha_allowed_block_types( $allowed_blocks ) {
 
    return array(
        'core/list',
        'core/paragraph',
        'core/heading',
        'cgb/text',
        'cgb/blockquote',
        'cgb/block-button',
        'cgb/column-sidebar',
        'cgb/column-text',
        'cgb/contact-cta',
        'cgb/intro',
        'cgb/linklist',
        'cgb/logos',
        'cgb/overview',
        'cgb/block-page-intro',
        'cgb/related',
        'cgb/share',
        'cgb/slider',
        'cgb/text',
        'cgb/usps',
        'cgb/widget-question',
        'cgb/widget-video',
        'cgb/employees',
        'cgb/jobapply',
        'cgb/vimeo',
        'cgb/youtube',
        'cgb/circle',
        'cgb/steps'
    );
 
}

add_action( 'admin_menu', 'fideo_add_admin_menu' );
add_action( 'admin_init', 'fideo_settings_init' );


function fideo_add_admin_menu(  ) { 

    add_options_page( 'Fideo', 'Fideo', 'manage_options', 'fideo', 'fideo_options_page' );

}


function fideo_settings_init(  ) { 

    register_setting( 'pluginPage', 'fideo_settings' );

    add_settings_section(
        'fideo_pluginPage_section', 
        'Video', 
        'fideo_settings_section_callback', 
        'pluginPage'
    );

    add_settings_field( 
        'fideo_slogan', 
        'Slogans', 
        'fideo_slogan_render', 
        'pluginPage', 
        'fideo_pluginPage_section' 
    );


}


function fideo_slogan_render(  ) { 

    $options = get_option( 'fideo_settings' );
    ?>
    <textarea cols='40' rows='5' name='fideo_settings[fideo_slogan]'><?php echo $options['fideo_slogan']; ?></textarea>
    <?php

}


function fideo_settings_section_callback(  ) { 

    echo __( '', 'fideo' );

}


function fideo_options_page(  ) { 

    ?>
    <form action='options.php' method='post'>

        <h2>Fideo</h2>

        <?php
        settings_fields( 'pluginPage' );
        do_settings_sections( 'pluginPage' );
        submit_button();
        ?>

    </form>
    <?php

}












/**
 * Generated by the WordPress Option Page generator
 * at http://jeremyhixon.com/wp-tools/option-page/
 */

class Fideo {
	private $fideo_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'fideo_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'fideo_page_init' ) );
	}

	public function fideo_add_plugin_page() {
		add_menu_page(
			'Fideo', // page_title
			'Fideo', // menu_title
			'manage_options', // capability
			'fideo', // menu_slug
			array( $this, 'fideo_create_admin_page' ), // function
			'dashicons-admin-generic', // icon_url
			3 // position
		);
	}

	public function fideo_create_admin_page() {
		$this->fideo_options = get_option( 'fideo_option_name' ); ?>

		<div class="wrap">
			<h2>Fideo</h2>
			<p></p>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'fideo_option_group' );
					do_settings_sections( 'fideo-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function fideo_page_init() {
		register_setting(
			'fideo_option_group', // option_group
			'fideo_option_name', // option_name
			array( $this, 'fideo_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'fideo_setting_section', // id
			'Settings', // title
			array( $this, 'fideo_section_info' ), // callback
			'fideo-admin' // page
		);

		add_settings_field(
			'slogans_0', // id
			'Slogans', // title
			array( $this, 'slogans_0_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'notificatie_balk_tonen_1', // id
			'Notificatie balk tonen', // title
			array( $this, 'notificatie_balk_tonen_1_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'notificatie_balk_tekst_2', // id
			'Notificatie balk tekst', // title
			array( $this, 'notificatie_balk_tekst_2_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'notificatie_balk_link_3', // id
			'Notificatie balk link', // title
			array( $this, 'notificatie_balk_link_3_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'notificatie_balk_knop_tekst_4', // id
			'Notificatie balk knop tekst', // title
			array( $this, 'notificatie_balk_knop_tekst_4_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'nieuwsbrief_bedanktmelding_5', // id
			'Nieuwsbrief bedanktmelding', // title
			array( $this, 'nieuwsbrief_bedanktmelding_5_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

        add_settings_field(
            'nieuwsbrief_titel_5', // id
            'Nieuwsbrief footer titel', // title
            array( $this, 'nieuwsbrief_titel_5_callback' ), // callback
            'fideo-admin', // page
            'fideo_setting_section' // section
        );

        add_settings_field(
            'email_contact', // id
            'Email contact formulier', // title
            array( $this, 'email_contact_callback' ), // callback
            'fideo-admin', // page
            'fideo_setting_section' // section
        );

        add_settings_field(
            'email_vacancy', // id
            'Email sollicitatie formulier', // title
            array( $this, 'email_vacancy_callback' ), // callback
            'fideo-admin', // page
            'fideo_setting_section' // section
        );

		add_settings_field(
			'usp_1_6', // id
			'USP 1', // title
			array( $this, 'usp_1_6_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_1_kleur_7', // id
			'USP 1 kleur', // title
			array( $this, 'usp_1_kleur_7_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_2_8', // id
			'USP 2', // title
			array( $this, 'usp_2_8_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_2_kleur_9', // id
			'USP 2 kleur', // title
			array( $this, 'usp_2_kleur_9_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_3_10', // id
			'USP 3', // title
			array( $this, 'usp_3_10_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_3_kleur_11', // id
			'USP 3 kleur', // title
			array( $this, 'usp_3_kleur_11_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_4_12', // id
			'USP 4', // title
			array( $this, 'usp_4_12_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_4_kleur_13', // id
			'USP 4 kleur', // title
			array( $this, 'usp_4_kleur_13_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_5_14', // id
			'USP 5', // title
			array( $this, 'usp_5_14_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_5_kleur_15', // id
			'USP 5 kleur', // title
			array( $this, 'usp_5_kleur_15_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_titel_16', // id
			'USP titel', // title
			array( $this, 'usp_titel_16_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_button_tekst_17', // id
			'USP button tekst', // title
			array( $this, 'usp_button_tekst_17_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);

		add_settings_field(
			'usp_button_link_18', // id
			'USP button link', // title
			array( $this, 'usp_button_link_18_callback' ), // callback
			'fideo-admin', // page
			'fideo_setting_section' // section
		);
	}

	public function fideo_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['slogans_0'] ) ) {
			$sanitary_values['slogans_0'] = esc_textarea( $input['slogans_0'] );
		}

		if ( isset( $input['notificatie_balk_tonen_1'] ) ) {
			$sanitary_values['notificatie_balk_tonen_1'] = $input['notificatie_balk_tonen_1'];
		}

		if ( isset( $input['notificatie_balk_tekst_2'] ) ) {
			$sanitary_values['notificatie_balk_tekst_2'] = sanitize_text_field( $input['notificatie_balk_tekst_2'] );
		}

		if ( isset( $input['notificatie_balk_link_3'] ) ) {
			$sanitary_values['notificatie_balk_link_3'] = sanitize_text_field( $input['notificatie_balk_link_3'] );
		}

		if ( isset( $input['notificatie_balk_knop_tekst_4'] ) ) {
			$sanitary_values['notificatie_balk_knop_tekst_4'] = sanitize_text_field( $input['notificatie_balk_knop_tekst_4'] );
		}

		if ( isset( $input['nieuwsbrief_bedanktmelding_5'] ) ) {
			$sanitary_values['nieuwsbrief_bedanktmelding_5'] = esc_textarea( $input['nieuwsbrief_bedanktmelding_5'] );
		}

        if ( isset( $input['nieuwsbrief_titel_5'] ) ) {
            $sanitary_values['nieuwsbrief_titel_5'] = esc_textarea( $input['nieuwsbrief_titel_5'] );
        }

		if ( isset( $input['usp_1_6'] ) ) {
			$sanitary_values['usp_1_6'] = esc_textarea( $input['usp_1_6'] );
		}

		if ( isset( $input['usp_1_kleur_7'] ) ) {
			$sanitary_values['usp_1_kleur_7'] = $input['usp_1_kleur_7'];
		}

		if ( isset( $input['usp_2_8'] ) ) {
			$sanitary_values['usp_2_8'] = esc_textarea( $input['usp_2_8'] );
		}

		if ( isset( $input['usp_2_kleur_9'] ) ) {
			$sanitary_values['usp_2_kleur_9'] = $input['usp_2_kleur_9'];
		}

		if ( isset( $input['usp_3_10'] ) ) {
			$sanitary_values['usp_3_10'] = esc_textarea( $input['usp_3_10'] );
		}

		if ( isset( $input['usp_3_kleur_11'] ) ) {
			$sanitary_values['usp_3_kleur_11'] = $input['usp_3_kleur_11'];
		}

		if ( isset( $input['usp_4_12'] ) ) {
			$sanitary_values['usp_4_12'] = esc_textarea( $input['usp_4_12'] );
		}

		if ( isset( $input['usp_4_kleur_13'] ) ) {
			$sanitary_values['usp_4_kleur_13'] = $input['usp_4_kleur_13'];
		}

		if ( isset( $input['usp_5_14'] ) ) {
			$sanitary_values['usp_5_14'] = esc_textarea( $input['usp_5_14'] );
		}

		if ( isset( $input['usp_5_kleur_15'] ) ) {
			$sanitary_values['usp_5_kleur_15'] = $input['usp_5_kleur_15'];
		}

		if ( isset( $input['usp_titel_16'] ) ) {
			$sanitary_values['usp_titel_16'] = sanitize_text_field( $input['usp_titel_16'] );
		}

		if ( isset( $input['usp_button_tekst_17'] ) ) {
			$sanitary_values['usp_button_tekst_17'] = sanitize_text_field( $input['usp_button_tekst_17'] );
		}

        if ( isset( $input['usp_button_link_18'] ) ) {
            $sanitary_values['usp_button_link_18'] = sanitize_text_field( $input['usp_button_link_18'] );
        }

        if ( isset( $input['email_contact'] ) ) {
            $sanitary_values['email_contact'] = sanitize_text_field( $input['email_contact'] );
        }

        if ( isset( $input['email_vacancy'] ) ) {
            $sanitary_values['email_vacancy'] = sanitize_text_field( $input['email_vacancy'] );
        }

		return $sanitary_values;
	}

	public function fideo_section_info() {
		
	}

	public function slogans_0_callback() {
		printf(
			'<textarea class="large-text" rows="5" name="fideo_option_name[slogans_0]" id="slogans_0">%s</textarea>',
			isset( $this->fideo_options['slogans_0'] ) ? esc_attr( $this->fideo_options['slogans_0']) : ''
		);
	}

	public function notificatie_balk_tonen_1_callback() {
		printf(
			'<input type="checkbox" name="fideo_option_name[notificatie_balk_tonen_1]" id="notificatie_balk_tonen_1" value="notificatie_balk_tonen_1" %s>',
			( isset( $this->fideo_options['notificatie_balk_tonen_1'] ) && $this->fideo_options['notificatie_balk_tonen_1'] === 'notificatie_balk_tonen_1' ) ? 'checked' : ''
		);
	}

	public function notificatie_balk_tekst_2_callback() {
		printf(
			'<input class="regular-text" type="text" name="fideo_option_name[notificatie_balk_tekst_2]" id="notificatie_balk_tekst_2" value="%s">',
			isset( $this->fideo_options['notificatie_balk_tekst_2'] ) ? esc_attr( $this->fideo_options['notificatie_balk_tekst_2']) : ''
		);
	}

	public function notificatie_balk_link_3_callback() {
		printf(
			'<input class="regular-text" type="text" name="fideo_option_name[notificatie_balk_link_3]" id="notificatie_balk_link_3" value="%s">',
			isset( $this->fideo_options['notificatie_balk_link_3'] ) ? esc_attr( $this->fideo_options['notificatie_balk_link_3']) : ''
		);
	}

	public function notificatie_balk_knop_tekst_4_callback() {
		printf(
			'<input class="regular-text" type="text" name="fideo_option_name[notificatie_balk_knop_tekst_4]" id="notificatie_balk_knop_tekst_4" value="%s">',
			isset( $this->fideo_options['notificatie_balk_knop_tekst_4'] ) ? esc_attr( $this->fideo_options['notificatie_balk_knop_tekst_4']) : ''
		);
	}

	public function nieuwsbrief_bedanktmelding_5_callback() {
		printf(
			'<textarea class="large-text" rows="5" name="fideo_option_name[nieuwsbrief_bedanktmelding_5]" id="nieuwsbrief_bedanktmelding_5">%s</textarea>',
			isset( $this->fideo_options['nieuwsbrief_bedanktmelding_5'] ) ? esc_attr( $this->fideo_options['nieuwsbrief_bedanktmelding_5']) : ''
		);
	}

    public function nieuwsbrief_titel_5_callback() {
        printf(
            '<textarea class="large-text" rows="5" name="fideo_option_name[nieuwsbrief_titel_5]" id="nieuwsbrief_titel_5">%s</textarea>',
            isset( $this->fideo_options['nieuwsbrief_titel_5'] ) ? esc_attr( $this->fideo_options['nieuwsbrief_titel_5']) : ''
        );
    }

    public function email_contact_callback() {
        printf(
            '<input class="regular-text" type="text" name="fideo_option_name[email_contact]" id="email_contact" value="%s">',
            isset( $this->fideo_options['email_contact'] ) ? esc_attr( $this->fideo_options['email_contact']) : ''
        );
    }

    public function email_vacancy_callback() {
        printf(
            '<input class="regular-text" type="text" name="fideo_option_name[email_vacancy]" id="email_vacancy" value="%s">',
            isset( $this->fideo_options['email_vacancy'] ) ? esc_attr( $this->fideo_options['email_vacancy']) : ''
        );
    }

	public function usp_1_6_callback() {
		printf(
			'<textarea class="large-text" rows="5" name="fideo_option_name[usp_1_6]" id="usp_1_6">%s</textarea>',
			isset( $this->fideo_options['usp_1_6'] ) ? esc_attr( $this->fideo_options['usp_1_6']) : ''
		);
	}

	public function usp_1_kleur_7_callback() {
		?> <select name="fideo_option_name[usp_1_kleur_7]" id="usp_1_kleur_7">
			<?php $selected = (isset( $this->fideo_options['usp_1_kleur_7'] ) && $this->fideo_options['usp_1_kleur_7'] === 'red') ? 'selected' : '' ; ?>
			<option value="red" <?php echo $selected; ?>>Rood</option>
			<?php $selected = (isset( $this->fideo_options['usp_1_kleur_7'] ) && $this->fideo_options['usp_1_kleur_7'] === 'yellow') ? 'selected' : '' ; ?>
			<option value="yellow" <?php echo $selected; ?>>Geel</option>
			<?php $selected = (isset( $this->fideo_options['usp_1_kleur_7'] ) && $this->fideo_options['usp_1_kleur_7'] === 'light-blue') ? 'selected' : '' ; ?>
			<option value="light-blue" <?php echo $selected; ?>>Licht blauw</option>
			<?php $selected = (isset( $this->fideo_options['usp_1_kleur_7'] ) && $this->fideo_options['usp_1_kleur_7'] === 'dark-blue') ? 'selected' : '' ; ?>
			<option value="dark-blue" <?php echo $selected; ?>>Donder blauw</option>
		</select> <?php
	}

	public function usp_2_8_callback() {
		printf(
			'<textarea class="large-text" rows="5" name="fideo_option_name[usp_2_8]" id="usp_2_8">%s</textarea>',
			isset( $this->fideo_options['usp_2_8'] ) ? esc_attr( $this->fideo_options['usp_2_8']) : ''
		);
	}

	public function usp_2_kleur_9_callback() {
		?> <select name="fideo_option_name[usp_2_kleur_9]" id="usp_2_kleur_9">
			<?php $selected = (isset( $this->fideo_options['usp_2_kleur_9'] ) && $this->fideo_options['usp_2_kleur_9'] === 'red') ? 'selected' : '' ; ?>
			<option value="red" <?php echo $selected; ?>>Rood</option>
			<?php $selected = (isset( $this->fideo_options['usp_2_kleur_9'] ) && $this->fideo_options['usp_2_kleur_9'] === 'yellow') ? 'selected' : '' ; ?>
			<option value="yellow" <?php echo $selected; ?>>Geel</option>
			<?php $selected = (isset( $this->fideo_options['usp_2_kleur_9'] ) && $this->fideo_options['usp_2_kleur_9'] === 'light-blue') ? 'selected' : '' ; ?>
			<option value="light-blue" <?php echo $selected; ?>>Licht blauw</option>
			<?php $selected = (isset( $this->fideo_options['usp_2_kleur_9'] ) && $this->fideo_options['usp_2_kleur_9'] === 'dark-blue') ? 'selected' : '' ; ?>
			<option value="dark-blue" <?php echo $selected; ?>>Donder blauw</option>
		</select> <?php
	}

	public function usp_3_10_callback() {
		printf(
			'<textarea class="large-text" rows="5" name="fideo_option_name[usp_3_10]" id="usp_3_10">%s</textarea>',
			isset( $this->fideo_options['usp_3_10'] ) ? esc_attr( $this->fideo_options['usp_3_10']) : ''
		);
	}

	public function usp_3_kleur_11_callback() {
		?> <select name="fideo_option_name[usp_3_kleur_11]" id="usp_3_kleur_11">
			<?php $selected = (isset( $this->fideo_options['usp_3_kleur_11'] ) && $this->fideo_options['usp_3_kleur_11'] === 'red') ? 'selected' : '' ; ?>
			<option value="red" <?php echo $selected; ?>>Rood</option>
			<?php $selected = (isset( $this->fideo_options['usp_3_kleur_11'] ) && $this->fideo_options['usp_3_kleur_11'] === 'yellow') ? 'selected' : '' ; ?>
			<option value="yellow" <?php echo $selected; ?>>Geel</option>
			<?php $selected = (isset( $this->fideo_options['usp_3_kleur_11'] ) && $this->fideo_options['usp_3_kleur_11'] === 'light-blue') ? 'selected' : '' ; ?>
			<option value="light-blue" <?php echo $selected; ?>>Licht blauw</option>
			<?php $selected = (isset( $this->fideo_options['usp_3_kleur_11'] ) && $this->fideo_options['usp_3_kleur_11'] === 'dark-blue') ? 'selected' : '' ; ?>
			<option value="dark-blue" <?php echo $selected; ?>>Donder blauw</option>
		</select> <?php
	}

	public function usp_4_12_callback() {
		printf(
			'<textarea class="large-text" rows="5" name="fideo_option_name[usp_4_12]" id="usp_4_12">%s</textarea>',
			isset( $this->fideo_options['usp_4_12'] ) ? esc_attr( $this->fideo_options['usp_4_12']) : ''
		);
	}

	public function usp_4_kleur_13_callback() {
		?> <select name="fideo_option_name[usp_4_kleur_13]" id="usp_4_kleur_13">
			<?php $selected = (isset( $this->fideo_options['usp_4_kleur_13'] ) && $this->fideo_options['usp_4_kleur_13'] === 'red') ? 'selected' : '' ; ?>
			<option value="red" <?php echo $selected; ?>>Rood</option>
			<?php $selected = (isset( $this->fideo_options['usp_4_kleur_13'] ) && $this->fideo_options['usp_4_kleur_13'] === 'yellow') ? 'selected' : '' ; ?>
			<option value="yellow" <?php echo $selected; ?>>Geel</option>
			<?php $selected = (isset( $this->fideo_options['usp_4_kleur_13'] ) && $this->fideo_options['usp_4_kleur_13'] === 'light-blue') ? 'selected' : '' ; ?>
			<option value="light-blue" <?php echo $selected; ?>>Licht blauw</option>
			<?php $selected = (isset( $this->fideo_options['usp_4_kleur_13'] ) && $this->fideo_options['usp_4_kleur_13'] === 'dark-blue') ? 'selected' : '' ; ?>
			<option value="dark-blue" <?php echo $selected; ?>>Donder blauw</option>
		</select> <?php
	}

	public function usp_5_14_callback() {
		printf(
			'<textarea class="large-text" rows="5" name="fideo_option_name[usp_5_14]" id="usp_5_14">%s</textarea>',
			isset( $this->fideo_options['usp_5_14'] ) ? esc_attr( $this->fideo_options['usp_5_14']) : ''
		);
	}

	public function usp_5_kleur_15_callback() {
		?> <select name="fideo_option_name[usp_5_kleur_15]" id="usp_5_kleur_15">
			<?php $selected = (isset( $this->fideo_options['usp_5_kleur_15'] ) && $this->fideo_options['usp_5_kleur_15'] === 'red') ? 'selected' : '' ; ?>
			<option value="red" <?php echo $selected; ?>>Rood</option>
			<?php $selected = (isset( $this->fideo_options['usp_5_kleur_15'] ) && $this->fideo_options['usp_5_kleur_15'] === 'yellow') ? 'selected' : '' ; ?>
			<option value="yellow" <?php echo $selected; ?>>Geel</option>
			<?php $selected = (isset( $this->fideo_options['usp_5_kleur_15'] ) && $this->fideo_options['usp_5_kleur_15'] === 'light-blue') ? 'selected' : '' ; ?>
			<option value="light-blue" <?php echo $selected; ?>>Licht blauw</option>
			<?php $selected = (isset( $this->fideo_options['usp_5_kleur_15'] ) && $this->fideo_options['usp_5_kleur_15'] === 'dark-blue') ? 'selected' : '' ; ?>
			<option value="dark-blue" <?php echo $selected; ?>>Donder blauw</option>
		</select> <?php
	}

	public function usp_titel_16_callback() {
		printf(
			'<input class="regular-text" type="text" name="fideo_option_name[usp_titel_16]" id="usp_titel_16" value="%s">',
			isset( $this->fideo_options['usp_titel_16'] ) ? esc_attr( $this->fideo_options['usp_titel_16']) : ''
		);
	}

	public function usp_button_tekst_17_callback() {
		printf(
			'<input class="regular-text" type="text" name="fideo_option_name[usp_button_tekst_17]" id="usp_button_tekst_17" value="%s">',
			isset( $this->fideo_options['usp_button_tekst_17'] ) ? esc_attr( $this->fideo_options['usp_button_tekst_17']) : ''
		);
	}

	public function usp_button_link_18_callback() {
		printf(
			'<input class="regular-text" type="text" name="fideo_option_name[usp_button_link_18]" id="usp_button_link_18" value="%s">',
			isset( $this->fideo_options['usp_button_link_18'] ) ? esc_attr( $this->fideo_options['usp_button_link_18']) : ''
		);
	}

}
if ( is_admin() )
	$fideo = new Fideo();

/* 
 * Retrieve this value with:
 * $fideo_options = get_option( 'fideo_option_name' ); // Array of All Options
 * $slogans_0 = $fideo_options['slogans_0']; // Slogans
 * $notificatie_balk_tonen_1 = $fideo_options['notificatie_balk_tonen_1']; // Notificatie balk tonen
 * $notificatie_balk_tekst_2 = $fideo_options['notificatie_balk_tekst_2']; // Notificatie balk tekst
 * $notificatie_balk_link_3 = $fideo_options['notificatie_balk_link_3']; // Notificatie balk link
 * $notificatie_balk_knop_tekst_4 = $fideo_options['notificatie_balk_knop_tekst_4']; // Notificatie balk knop tekst
 * $nieuwsbrief_bedanktmelding_5 = $fideo_options['nieuwsbrief_bedanktmelding_5']; // Nieuwsbrief bedanktmelding
 * $usp_1_6 = $fideo_options['usp_1_6']; // USP 1
 * $usp_1_kleur_7 = $fideo_options['usp_1_kleur_7']; // USP 1 kleur
 * $usp_2_8 = $fideo_options['usp_2_8']; // USP 2
 * $usp_2_kleur_9 = $fideo_options['usp_2_kleur_9']; // USP 2 kleur
 * $usp_3_10 = $fideo_options['usp_3_10']; // USP 3
 * $usp_3_kleur_11 = $fideo_options['usp_3_kleur_11']; // USP 3 kleur
 * $usp_4_12 = $fideo_options['usp_4_12']; // USP 4
 * $usp_4_kleur_13 = $fideo_options['usp_4_kleur_13']; // USP 4 kleur
 * $usp_5_14 = $fideo_options['usp_5_14']; // USP 5
 * $usp_5_kleur_15 = $fideo_options['usp_5_kleur_15']; // USP 5 kleur
 * $usp_titel_16 = $fideo_options['usp_titel_16']; // USP titel
 * $usp_button_tekst_17 = $fideo_options['usp_button_tekst_17']; // USP button tekst
 * $usp_button_link_18 = $fideo_options['usp_button_link_18']; // USP button link
 */



include('api/index.php');