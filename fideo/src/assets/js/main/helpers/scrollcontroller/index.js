export default {
    disableScroll() {
    // remove active event listeners
        this.enableScroll();

        // add scroll event listener
        $('body').on('mousewheel.scroll DOMMouseScroll.scroll scroll.scroll', () => false);
    },

    enableScroll() {
    // remove active event listeners
        $('body').off('mousewheel.scroll DOMMouseScroll.scroll scroll.scroll');
    },
};
