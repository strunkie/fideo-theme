export default {
    Spaceless(strings, ...values) {
        return this.SingleLine(strings, ...values).replace(/\s/g, '');
    },

    SingleLine(strings, ...values) {
        let output = '';
        for (let i = 0; i < values.length; i++) {
            output += strings[i] + values[i];
        }
        output += strings[values.length];

        // Split on newlines.
        const lines = output.split(/(?:\r\n|\n|\r)/);

        // Rip out the leading whitespace.
        return lines.map(line => line.replace(/^\s+/gm, '')).join(' ').trim();
    },
};
