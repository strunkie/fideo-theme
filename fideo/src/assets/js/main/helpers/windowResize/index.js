import Events from '@/helpers/events';

export default new class WindowResize {
    constructor() {
        const events = new Events();
        events.bind(this);

        this.resizeTimeout = null;

        this.addEventListeners();
    }

    addEventListeners() {
        $(window).on('resize.windowResize', () => {
            clearTimeout(this.resizeTimeout);

            this.resizeTimeout = setTimeout(() => {
                this.trigger('resize');
            }, 100);
        });
    }

    clearAll() {
        this.events = [];
    }
}();
