import '../../sass/index.scss';
import _ from 'lodash';
import Router from '@/core/router';
import ModuleAjaxform from '@/modules/ajaxform';
import ModuleMainnav from '@/modules/mainnav';
import ModuleSlider from '@/modules/slider';
import ModuleEmployeesSlider from '@/modules/employeesSlider';
import ModuleLogoCarrousel from '@/modules/logoCarrousel';
import ModuleGif from '@/modules/gif';
import ModuleArticle from '@/modules/article';
import ModuleNotificationbar from '@/modules/notificationbar';
import ModulePayoff from '@/modules/payoff';
import ModuleFilters from '@/modules/filters';
import ModuleFaq from '@/modules/faq';
import ModuleBenefits from '@/modules/benefits';
import ModuleHeaderVideo from '@/modules/headerVideo';
import ModuleScrollToContent from '@/modules/scrolltocontent';
import ModuleShare from '@/modules/share';
import ModuleFideoPopup from '@/modules/fideopopup';
import ModuleNewsletter from '@/modules/ajaxform/newsletter';
import ModuleSearch from '@/modules/search';
import { TweenMax } from 'gsap';

window._ = {
    escape: _.escape,
};

Router.setup({
    options: {
        pagecontainer: '.page-container',
        defaultTransition: 'fade',
        ajaxloading: false,
    },
    modules: [
        {
            class: ModuleMainnav,
            element: '.topbar',
            single: true,
        },
        {
            class: ModuleSlider,
            element: '.slider',
            single: true,
        },
        {
            class: ModuleEmployeesSlider,
            element: '.employees-slider-container',
            single: true,
        },
        {
            class: ModuleLogoCarrousel,
            element: '.partners',
            single: true,
        },
        {
            class: ModuleGif,
            element: '[hover-video]',
            single: true,
        },
        {
            class: ModuleArticle,
            element: '.article',
            single: true,
        },
        {
            class: ModuleAjaxform,
            element: '[ajaxform]',
        },
        {
            class: ModuleNewsletter,
            element: '[newsletter]',
        },
        {
            class: ModulePayoff,
            element: '.payoff',
        },
        {
            class: ModuleFilters,
            element: '.filterbar',
            single: true,
        },
        {
            class: ModuleFaq,
            element: '.faq',
            single: true,
        },
        {
            class: ModuleBenefits,
            element: '.benefits',
            single: true,
        },
        {
            load: () => import('@/modules/blog'),
            element: '[blog]',
        },
        {
            class: ModuleHeaderVideo,
            element: '.bg-video',
        },
        {
            class: ModuleScrollToContent,
            element: '[scroll-to-content]',
        },
        {
            class: ModuleShare,
            element: document,
        },
        {
            class: ModuleFideoPopup,
            element: document,
        },
        {
            class: ModuleNotificationbar,
            element: '.notificationbar',
        },
        {
            class: ModuleSearch,
            element: '.searchbar',
        },
    ],
});

TweenMax.to('.header--content', 100, { backgroundPosition: '-1000px center', repeat: -1 });
TweenMax.to('.mainnav', 100, { backgroundPosition: '-1000px center', repeat: -1 });
TweenMax.to('.bg-video__cover--pattern', 100, { backgroundPosition: '-1000px center', repeat: -1 });

const $elements = $('section:not(.contact):not([no-fade]), .benefits, footer');
TweenMax.set($elements, { y: 80, autoAlpha: 0 });

$(window).on('scroll', () => {
    $elements.each((index, el) => {
        const $el = $(el);

        if ($el.attr('animated') === 1) {
            return;
        }

        if (($el.offset().top - (parseInt($(window).height(), 10) * 0.9)) < $(window).scrollTop()) {
            TweenMax.to($el, 0.8, { y: 0, autoAlpha: 1 });
            $el.attr('animated', 1);
        }
    });
}).trigger('scroll');
