import Core from '@/core/module';
import { TweenMax } from 'gsap';

export default class Gif extends Core {
    init() {
        if ($(window).width() > 800) {
            this.addEventListeners();
            this.$img = this.$el.find('img[gif-url]');
            this.gifSrc = this.$img.attr('gif-url');
            this.originalSrc = this.$img.attr('src');
        }
    }

    addEventListeners() {
        this.$el.on('mouseenter', () => {
            this.$el.find('video')[0].play();

            TweenMax.to(this.$el.find('img'), 0.2, { autoAlpha: 0 });
        });

        this.$el.on('mouseleave', () => {
            this.$el.find('video')[0].pause();

            TweenMax.to(this.$el.find('img'), 0.2, { autoAlpha: 1 });
        });
    }
}
