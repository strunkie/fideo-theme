import Core from '@/core/module';
import 'slick-carousel';

export default class Slider extends Core {
    init() {
        this.slider = this.$el.find('.slider__slides').slick({
            arrows: false,
        });

        this.addEventListeners();
    }

    addEventListeners() {
        this.$el.on('click', '.slider__button--prev', (e) => {
            e.preventDefault();
            this.slider.slick('slickPrev');
        });

        this.$el.on('click', '.slider__button--next', (e) => {
            e.preventDefault();
            this.slider.slick('slickNext');
        });
    }
}
