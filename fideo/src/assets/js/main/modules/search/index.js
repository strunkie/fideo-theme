import Core from '@/core/module';

export default class _Example extends Core {
    init() {
        this.addEventListeners();
    }

    addEventListeners() {
        $('.topbar__search').on('click', () => {
            $('.searchbar').toggleClass('searchbar--open');
        });
    }
}
