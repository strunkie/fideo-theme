import Core from '@/core/module';
import { TweenMax, Power0 } from 'gsap';

export default class logoCarrousel extends Core {
    init() {
        this.addEventListeners();
    }

    addEventListeners() {
        const $panel = this.$el.find('.partners__panel').clone();
        this.$el.find('.partners__inner').append($panel);

        this.animation = TweenMax.to(
            this.$el.find('.partners__inner'),
            this.$el.find('.partners__panel').width() * 0.02,
            {
                x: -(this.$el.find('.partners__panel').width()),
                ease: Power0.easeNone,
                repeat: -1,
            },
        );

        if ($(window).width() > 900) {
            this.$el.find('.partners__panel').on('mouseenter', () => {
                this.animation.pause();
            });

            this.$el.find('.partners__panel').on('mouseleave', () => {
                this.animation.play();
            });
        }
    }
}
