import { TweenMax } from 'gsap';
import Core from '@/core/module';

export default class Scrolltocontent extends Core {
    init() {
        this.$el.on('click', (e) => {
            e.preventDefault();

            TweenMax.to('body, html', 0.6, {
                scrollTop: $('.header').height(),
            });
        });
    }
}
