import Core from '@/core/module';
import 'slick-carousel';

export default class EmployeesSlider extends Core {
    init() {
        this.slider = this.$el.find('.employees-slider').slick({
            arrows: false,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 430,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
            ],
        });

        this.addEventListeners();
    }

    addEventListeners() {
        this.$el.on('click', '.sliderbuttons__button--prev', (e) => {
            e.preventDefault();
            this.slider.slick('slickPrev');
        });

        this.$el.on('click', '.sliderbuttons__button--next', (e) => {
            e.preventDefault();
            this.slider.slick('slickNext');
        });
    }
}
