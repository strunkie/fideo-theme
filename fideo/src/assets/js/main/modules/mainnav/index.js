import Core from '@/core/module';
import { TweenMax, TimelineMax } from 'gsap';

export default class Mainnav extends Core {
    init() {
        this.options.open = false;
        this.isAnimating = false;

        this.addEventListeners();
    }

    addEventListeners() {
        this.$el.on('click', '.hamburger', (e) => {
            e.preventDefault();

            let size = window.innerHeight;

            if (window.innerWidth > window.innerHeight) {
                size = window.innerWidth;
            }

            size *= 2;

            $('.circle-overlay-yellow').css({ width: size, height: size });
            $('.circle-overlay-blue').css({ width: size, height: size });

            if (this.isAnimating) {
                return;
            }

            this.isAnimating = true;

            if (this.options.open) {
                this.close();
            } else {
                this.open();
            }

            this.options.open = !this.options.open;
        });

        $(window).on('scroll.topbar', () => {
            if ($(window).scrollTop() > 50) {
                this.stickTopbar();
            } else {
                this.unstickTopbar();
            }
        }).trigger('scroll.topbar');
    }

    stickTopbar() {
        $('.topbar').addClass('topbar--sticky');
    }

    unstickTopbar() {
        $('.topbar').removeClass('topbar--sticky');
    }

    open() {
        $('.topbar').addClass('topbar--mainnav-open');
        $('.topbar').addClass('topbar--open');

        const tl = new TimelineMax({
            onComplete: () => {
                this.isAnimating = false;

                $('.mainnav').show();

                $('.circle-overlay-yellow').hide();
                TweenMax.to('.circle-overlay-blue', 0.2, { autoAlpha: 0 });

                TweenMax.staggerFromTo(
                    '.mainnav__items--main, .mainnav__items--sub, .mainnav__contact',
                    0.4,
                    {
                        y: 80,
                        autoAlpha: 0,
                    },
                    {
                        y: 0,
                        autoAlpha: 1,
                    },
                    0.1,
                );
            },
        });

        tl.to('.circle-overlay-yellow', 0.3, { scale: 1 });
        tl.to('.circle-overlay-blue', 0.3, { scale: 1 }, 0.1);
    }

    close() {
        $('.topbar').removeClass('topbar--mainnav-open');
        $('.topbar').removeClass('topbar--open');

        const tl = new TimelineMax({
            onComplete: () => {
                this.isAnimating = false;
            },
        });

        tl.to('.circle-overlay-blue', 0.2, { autoAlpha: 1 });
        tl.add(() => {
            $('.circle-overlay-yellow').show();
            $('.mainnav').hide();
        });
        tl.addLabel('removeCircles');
        tl.to('.circle-overlay-blue', 0.3, { scale: 0 }, 'removeCircles');
        tl.to('.circle-overlay-yellow', 0.3, { scale: 0 }, 'removeCircles+=0.1');
    }
}
