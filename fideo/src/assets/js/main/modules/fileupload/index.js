import Core from '@/core/module';
import Template from './template.html';

export default class Fileupload extends Core {
    init() {
        if (this.$el.attr('type') !== 'file') {
            console.log('input for fileupload is not a file type!');
            return;
        }

        const $template = $(Template({
            tv: {
                name: this.options.name,
                url: this.options.url,
                placeholder: this.options.placeholder || 'Bestand selecteren',
                multiple: this.options.multiple,
            },
        }));

        this.$el.replaceWith($template);
        this.$el = $template;

        this.$el.on('change', 'input', (e) => {
            const filename = e.currentTarget.value.replace(/^.*\\/, '');

            if (filename) {
                this.$el.addClass('file--hasfile');
                this.$el.find('.fileupload__filename .fileupload__name').text(filename);
            } else {
                this.$el.find('[delete]').trigger('click');
            }
        });

        this.addEventListeners();
    }

    addEventListeners() {
        this.$el.on('click', '[delete]', () => {
            this.$el.find('[filename]').text('');
            this.$el.find('input').val('');

            this.$el.removeClass('file--hasfile');

            this.$el.find('input').val('');
            this.$el.find('.fileupload__filename .fileupload__name').text('');

            if (this.options.extraField) {
                this.$el.remove();
            }

            this.trigger('remove');
        });
    }
}
