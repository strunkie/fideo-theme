import Core from '@/core/module';
import Packery from 'packery';

export default class Blog extends Core {
    init() {
        this.packery = new Packery(this.$el[0]);
    }
}
