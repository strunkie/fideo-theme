import Core from '@/core/module';

export default class _Example extends Core {
    init() {
        this.addEventListeners();
    }

    addEventListeners() {
        $('[share]').on('click', (e) => {
            const $el = $(e.currentTarget);

            window.open($el.attr('href'), 'Delen', 'width=550,height=600');
        });
    }
}
