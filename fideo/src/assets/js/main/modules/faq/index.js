import Core from '@/core/module';
import { TweenMax } from 'gsap';

export default class Faq extends Core {
    init() {
        this.addEventListeners();
    }

    addEventListeners() {
        this.$el.on('click', '.faq__button', (e) => {
            const $el = $(e.currentTarget);
            const $item = $el.closest('.faq__item');

            if ($item.hasClass('faq__item--open')) {
                TweenMax.to($item.find('.faq__content'), 0.4, { height: 0 });
            } else {
                TweenMax.to($item.find('.faq__content'), 0.4, { height: $item.find('.faq__content')[0].scrollHeight });
            }

            setTimeout(() => {
                $item.toggleClass('faq__item--open');
            }, 10);
        });
    }
}
