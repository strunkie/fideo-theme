export default class SelectHandler {
    constructor($el) {
        this.$el = $el;

        this.addEventListeners();
    }

    addEventListeners() {
        this.$el.on('change', (e) => {
            const href = $(e.currentTarget).find('option:selected').attr('href');

            if (href) {
                window.location = href;
            }
        });
    }
}
