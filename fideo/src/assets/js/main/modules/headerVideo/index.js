import Core from '@/core/module';
import Vimeo from '@vimeo/player';

export default class _Example extends Core {
    init() {
        this.videoID = this.$el.attr('video');

        if (!this.videoID) {
            return;
        }

        const iframe = this.$el.find('#tv');
        const player = new Vimeo(iframe, {
            autoplay: true,
            id: this.videoID,
            title: false,
            loop: true,
            muted: true,
            autopause: false,
        });

        player.on('loaded', () => {
            this.$el.addClass('bg-video--loaded');
            this.addEventListeners();
        });

        setInterval(() => {
            player.setVolume(0);
        }, 100);
    }

    addEventListeners() {
        $(window).on('resize.video', () => {
            this.vidRescale();
        }).trigger('resize.video');
    }

    vidRescale() {
        const width = this.$el.width() + 200;
        const height = this.$el.height() + 200;

        if (width / height > 16 / 9) {
            this.$el.find('iframe').css({
                width,
                height: (width / 16) * 9,
            });
        } else {
            this.$el.find('iframe').css({
                width: (height / 9) * 16,
                height,
            });
        }
    }
}
