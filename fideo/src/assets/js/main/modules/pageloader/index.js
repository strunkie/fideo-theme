import { TweenMax, Power4 } from 'gsap';
import _$loader from './loader.html';

export default new class Pageloader {
    constructor() {
        this.$el = $(_$loader());

        $('body').append(this.$el);
    }

    start() {
        this.goTo(50);
        TweenMax.set(this.$el.find('.pageloader__bar'), { width: 0, autoAlpha: 1, top: -4 });
    }

    goTo(percentage) {
        TweenMax.to(this.$el, 0.1, { top: 0, autoAlpha: 1 });
        TweenMax.to(this.$el.find('.pageloader__bar'), 3, { width: `${percentage}%`, ease: Power4.easeOut });
    }

    end(callback) {
        TweenMax.to(this.$el.find('.pageloader__bar'), 0.5, {
            width: '100%',
            onComplete: () => {
                callback();
            },
        });
    }

    hide() {
        TweenMax.to(this.$el.find('.pageloader__bar'), 0.2, { autoAlpha: 0 });
    }
}();
