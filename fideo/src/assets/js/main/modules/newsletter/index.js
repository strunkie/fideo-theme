import Core from '@/core/module';
import Cookie from '@/helpers/cookie';
import GA from '@/helpers/ga';
import AjaxForm from '@/modules/ajaxform';

export default class Newsletter extends Core {
    init() {
        this.setDefaults({
            close: '.close',
            defaultTimeout: 10,
        });

        this.runTimer();
        this.addEventListeners();
    }

    addEventListeners() {
        this.$el.on('click', this.options.close, (e) => {
            e.preventDefault();

            this.close();
        });

        this.ajaxform = new AjaxForm(this.$el.find('form'));

        this.ajaxform.on('success', (data) => {
            this.$el.find('form').html(data.message);

            GA.pageview({
                path: '/nieuwsbrief/bedankt',
            });

            Cookie.set('newsletter_hide', {
                expiration: 365,
                value: 1,
            });
        });
    }

    removeEventListeners() {
        this.$el.off('click');
    }

    runTimer() {
        setTimeout(() => {
            this.removeEventListeners();
        }, 1000);

        if (!Cookie.get('newsletter_hide', Boolean)) {
            if (Cookie.get('newsletter_timeout', Number) === 0) {
                this.open();
            } else {
                Cookie.set('newsletter_animate', {
                    expiration: 1,
                    value: 1,
                });

                let currentTimeout = Cookie.get('newsletter_timeout', Number);

                if (!currentTimeout || currentTimeout < 0) {
                    currentTimeout = this.options.defaultTimeout;
                }

                const newsletterTimer = setInterval(() => {
                    if (currentTimeout === 0) {
                        clearInterval(newsletterTimer);

                        this.open();
                    }

                    Cookie.set('newsletter_timeout', {
                        expiration: 1,
                        value: currentTimeout,
                    });

                    currentTimeout -= 1;
                }, 1000);
            }
        } else {
            $(this).remove();
        }
    }

    open(noanimate) {
        if (noanimate) {
            this.$el.css('bottom', 0).show();
            return false;
        }

        this.$el.css('bottom', -this.$el.height()).show();
        this.$el.animate({ bottom: 0 }, 350);

        return this;
    }

    close() {
        this.$el.animate({ bottom: -this.$el.height() }, 350, () => {
            Cookie.set('newsletter_hide', {
                expiration: 1,
                value: 1,
            });

            this.$el.remove();
        });
    }
}
