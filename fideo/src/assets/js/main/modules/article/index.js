import Core from '@/core/module';
import SplitText from '@/vendor/gsap/SplitText';

export default class Article extends Core {
    init() {
        this.addEventListeners();
    }

    addEventListeners() {
        $(window).on('resize.splittext', () => {
            this.cutText();

            setTimeout(() => {
                this.cutText();
            }, 100);
        }).trigger('resize.splittext');


        setTimeout(() => {
            this.cutText();
        }, 1000);

        $(window).on('load', () => {
            this.cutText();
        });
    }

    cutText() {
        if (this.splittext) {
            this.splittext.revert();
        }

        if (window.innerWidth >= 640) {
            this.splittext = new SplitText(
                this.$el.find('.article__title'),
                {
                    type: 'words, lines',
                    linesClass: 'article__title__line',
                },
            );
            this.$el.find('.article__title__line').wrapInner('<div></div>');
        }
    }

    removeClipText() {
        if (this.splittext) {
            this.splittext.revert();
        }
    }
}
