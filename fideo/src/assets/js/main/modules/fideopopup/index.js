import Core from '@/core/module';
import { TweenMax } from 'gsap';

export default class Fidepopup extends Core {
    init() {
        this.addEventListeners();
        this.$popup = this.$el.find('.video-popup');
    }

    addEventListeners() {
        $(document).on('click', '[video-popup]', (e) => {
            e.preventDefault();

            const $el = $(e.currentTarget);
            const videoID = $el.attr('video-popup');

            this.showPopup(videoID);
        });

        $(document).on('click', '[close-video-popup]', (e) => {
            e.preventDefault();
            this.closePopup();
        });
    }

    showPopup(videoID) {
        this.$popup.find('iframe').attr('src', `https://player.vimeo.com/video/${videoID}?autoplay=true&rel=0`);
        TweenMax.fromTo(this.$popup, 0.4, { display: 'flex', autoAlpha: 0 }, { autoAlpha: 1 });
        // this.$popup.
    }

    closePopup() {
        TweenMax.to(this.$popup, 0.4, { autoAlpha: 0,
            onComplete: () => {
                this.$popup.find('iframe').attr('src', '');
                this.$popup.css({
                    display: 'none',
                });
            } });
    }
}
