import Core from '@/core/module';
import { TweenMax } from 'gsap';

export default class Notificationbar extends Core {
    init() {
        const barStorage = window.localStorage.getItem('hideNotificationBar');

        const $button = this.$el.find('.notificationbar__icon');

        this.negativeBarHeight = -parseInt(this.$el.outerHeight(), 10);

        if (barStorage === null) {
            TweenMax.fromTo(
                this.$el,
                0.4,
                {
                    bottom: this.negativeBarHeight,
                },
                {
                    bottom: 0,
                    display: 'flex',
                },
            );
        }

        $button.on('click', (e) => {
            e.preventDefault();
            this.hideBar();
        });
    }

    hideBar() {
        window.localStorage.setItem('hideNotificationBar', 1);

        TweenMax.to(
            this.$el,
            0.4,
            {
                bottom: this.negativeBarHeight,
                onComplete: () => {
                    this.$el.css({ display: 'none' });
                },
            },
        );
    }
}
