import Core from '@/core/module';
import { TweenMax, TimelineMax } from 'gsap';

export default class Payoff extends Core {
    init() {
        this.activeItem = parseInt(window.localStorage.getItem('activeSloganItem'), 10) || 0;

        TweenMax.set(this.$el.find('.payoff__bottom > div'), { top: -15 });
        TweenMax.set(this.$el.find(`.payoff__bottom > div:eq(${this.activeItem})`), { top: 0 });

        setInterval(() => {
            this.animateItem();
        }, 2000);
    }

    animateItem() {
        const nrOfItems = this.$el.find('.payoff__bottom > div').length;
        let newValue = this.activeItem + 1;

        if (newValue >= nrOfItems) {
            newValue = 0;
        }

        this.activeItem = newValue;
        window.localStorage.setItem('activeSloganItem', newValue);

        new TimelineMax();
        TweenMax.to(this.$el.find('.payoff__bottom > div'), 0.6, { top: -15, onComplete: () => {} });
        TweenMax.fromTo(
            this.$el.find(`.payoff__bottom > div:eq(${this.activeItem})`),
            0.6,
            {
                top: 15,
            },
            {
                top: 0,
            },
        );
    }
}
