import Core from '@/core/module';

export default class Filters extends Core {
    init() {
        this.addEventListeners();

        if (window.location.search) {
            setTimeout(() => {
                $(window).scrollTop($('.filterbar').closest('section').offset().top);
            }, 100);
        }
    }

    addEventListeners() {
        this.$el.on('change', 'select', () => {
            this.reloadPage();
        });
    }

    reloadPage() {
        clearInterval(this.refreshTimeout);

        this.refreshTimeout = setTimeout(() => {
            this.$el.submit();
        }, 1500);
    }
}
