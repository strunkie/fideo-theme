import Core from '@/core/module';
import GA from '@/helpers/ga';

export default class Mobilemenu extends Core {
    init() {
        this.setDefaults({
            container: '.mobile-nav',
            toggleElement: '.mobile-nav__toggle',
            bg: '.mobile-nav-bg',
            overlay: '.mobile-nav-overlay',
            isOpen: false,
        });

        this.addEventListeners();
    }

    addEventListeners() {
        $(this.options.toggleElement).on('click.mobileMenu', (e) => {
            e.preventDefault();

            this.toggle();
        });

        $(this.options.overlay).on('click.mobileMenu', (e) => {
            e.preventDefault();

            this.close();
        });
    }

    removeEventListeners() {
        $(this.options.toggleElement).off('click.mobileMenu');
        $(this.options.overlay).off('click.mobileMenu');
    }

    toggle() {
        if (this.options.isOpen) {
            this.close();

            this.options.isOpen = false;
        } else {
            this.open();

            this.options.isOpen = true;
        }
    }

    open() {
        if (this.options.isOpen) {
            return;
        }

        this.options.isOpen = true;
        $(this.options.overlay).stop().show().animate({
            opacity: 0.5,
        }, 600);

        $(this.options.container).addClass('show');
        $(this.options.bg).addClass('show');

        GA.event({
            category: 'mobile menu',
            action: 'click',
            label: 'open',
        });
    }

    close() {
        if (!this.options.isOpen) {
            return;
        }

        this.options.isOpen = false;
        $(this.options.overlay).stop().animate({
            opacity: 0,
        }, 600, () => {
            $(this.options.overlay).hide();
        });

        $(this.options.container).removeClass('show');
        $(this.options.bg).removeClass('show');

        GA.event({
            category: 'mobile menu',
            action: 'click',
            label: 'close',
        });
    }
}
