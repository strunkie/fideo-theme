import Core from '@/core/module';
import { TimelineMax, Back } from 'gsap';
import SplitText from '@/vendor/gsap/SplitText';

export default class Benefits extends Core {
    init() {
        this.currentSlide = 0;
        this.playAnimation(0, true);
    }

    playAnimation(indexBenefitsText, init = false) {
        const text = $('.benefits__text').eq(indexBenefitsText);
        text.css({ display: 'block' }).eq(indexBenefitsText);

        const backgroundClass = text.attr('background-class');
        text.closest('section').attr('class', backgroundClass).addClass('benefits');

        const splitLines = new SplitText(text, { type: 'lines', linesClass: 'line' });

        text.find('.line').wrap('<div class="line__wrapper">');

        const tl = new TimelineMax({
            onComplete: () => {
                this.currentSlide += 1;
                text.css({ display: 'none' });

                splitLines.revert();

                if (this.currentSlide >= $('.benefits__text').length) {
                    this.currentSlide = 0;
                }

                this.playAnimation(this.currentSlide);
            },
        });

        if (init === false) {
            tl.staggerFromTo(
                splitLines.lines,
                0.4,
                {
                    y: '100%',
                    autoAlpha: 0,
                },
                {
                    y: '0%',
                    autoAlpha: 1,
                    ease: Back.easeOut.config(1),
                },
                0.05,
            );
        }

        tl.staggerTo(splitLines.lines, 0.4, { y: '-100%', autoAlpha: 0, delay: 2, ease: Back.easeIn.config(1) }, 0.05);
    }
}
