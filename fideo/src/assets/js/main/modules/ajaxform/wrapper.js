import ModuleAjaxform from '@/modules/ajaxform';
import { TweenMax } from 'gsap';
import _ from 'lodash';

export default class AjaxForm extends ModuleAjaxform {
    addEventListeners() {
        super.addEventListeners();

        this.$el.on('click', '[nextstep]', (e) => {
            e.preventDefault();

            const $el = $(e.currentTarget);
            const $nextStep = $el.closest('.form__step').next();

            TweenMax.to('html, body', 0.55, {
                scrollTop: $nextStep.offset().top - 100,
            });

            $el.closest('.formfield--button').remove();

            TweenMax.to($nextStep, 0.55, {
                height: $nextStep[0].scrollHeight,
                onComplete: () => {
                    $nextStep.css({
                        height: 'auto',
                    });
                },
            });
        });

        this.checkChecked(true);

        this.$el.on('change', 'input[type="checkbox"], input[type="radio"]', () => {
            this.checkChecked();
        });
    }

    static checkChecked(noAnimate) {
        _.each($('form [form-show-if]'), (ifElement) => {
            const $el = $(ifElement);
            const data = JSON.parse($el.attr('form-show-if'));

            $el.css('overflow', 'hidden');

            if ($(`input[fieldid="${data.fieldid}"][value="${data.value}"]:checked`).length > 0) {
                TweenMax.to($el, 0.2, {
                    height: $el[0].scrollHeight,
                    onComplete: () => {
                        $el.css('height', 'auto');
                    },
                });
            } else if (noAnimate) {
                TweenMax.to($el, 0, { height: 0 });
            } else {
                TweenMax.to($el, 0.2, { height: 0 });
            }
        });
    }
}
