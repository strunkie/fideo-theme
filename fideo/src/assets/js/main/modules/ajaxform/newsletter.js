import ModuleAjaxform from '@/modules/ajaxform';

export default class Newsletter extends ModuleAjaxform {
    addEventListeners() {
        super.addEventListeners();

        $('.newsletter').on('click', '[footer-newsletter="start"]', (e) => {
            e.preventDefault();

            $('.newsletter').find('.newsletter__step').hide();
            $('.newsletter').find('.newsletter__step[form]').show();
        });
    }

    onSuccess() {
        $('.newsletter').find('.newsletter__step').hide();
        $('.newsletter').find('.newsletter__step[success]').show();
    }
}
