import _ from 'lodash';
import Events from '@/helpers/events';

export default new class Router {
    constructor() {
        const events = new Events();
        events.bind(this);

        this.options = {
        };

        this.modules = [];
        this.runningModules = [];
    }

    setup(data) {
        _.extend(this.options, data.options);
        this.addModules(data.modules);
    }

    addModules(modules) {
        this.modules = [this.modules, ...modules];

        this.fireModules();
    }

    fireModules() {
        this.off('afterLoad');

        this.destroyModules();

        _.each(this.modules, (module) => {
            if ($(module.element).length > 0) {
                if (module.single) {
                    _.each($(module.element), (el) => {
                        this.fireModule(el, module);
                    });
                } else {
                    this.fireModule(module.element, module);
                }
            }
        });
    }

    fireModule(element, module) {
        if (typeof module.class === 'function') {
            const ModuleClass = module.class;
            const createdModule = new ModuleClass(element, module.options);

            this.runningModules.push(createdModule);
        } else if (typeof module.load === 'function') {
            (async () => {
                const LoadedModule = await module.load();
                const ModuleClass = LoadedModule.default;
                const createdModule = new ModuleClass(element, module.options);
                this.runningModules.push(createdModule);
            })();
        }
    }

    destroyModules() {
        this.trigger('destroy');

        _.each(this.runningModules, (module) => {
            if (typeof module.removeEventListeners === 'function' && module.$el) {
                module.removeEventListeners();
            }
        });

        this.runningModules = [];
    }

    updateModule(moduleName) {
        _.each(this.modules, (module) => {
            if (moduleName === module.name) {
                const ModuleClass = module.class;
                const runningModule = new ModuleClass(module.element, module.options);

                this.runningModules.push(runningModule);
            }
        });
    }
}();
