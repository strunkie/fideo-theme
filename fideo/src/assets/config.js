const { argv } = require('yargs');

const env = process.env.NODE_ENV || 'development';

const options = {
    // js files
    webpack: {
        entry: {
            app: ['babel-polyfill', './assets/js/main/'],
        },
        dest: './../assets/',
    },

    images: {
        src: './assets/img/**/*',
        dest: './wordpress/wp-content/themes/fideo/assets/img/',
    },
};

module.exports = options;
