const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const UglifyJS = require('uglify-es');
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ManifestPlugin = require('webpack-manifest-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const config = require('./assets/config.js');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

const env = process.env.NODE_ENV || 'development';
const debug = env !== 'production';

let pathsToClean = [
    'js', 'css'
];

module.exports = {
    mode: env,
    cache: true,
    entry: ['babel-polyfill', './assets/js/main/'],
    // enable sourcemaps
    devtool: debug ? 'cheap-module-source-map' : 'hidden-source-map',
    output: {
        publicPath: '/wp-content/themes/fideo/assets/',
        path: path.resolve(config.webpack.dest),
        filename: 'js/main.[hash].js',
        chunkFilename: 'js/[name].[hash].bundle.js'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(env)
            }
        }),
        new MiniCssExtractPlugin({
            filename: "css/main.[hash].css",
            chunkFilename: "css/bundle.[hash].[id].css"
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery'
        }),
        new ManifestPlugin({

        }),
        new StyleLintPlugin({
            files: ['assets/sass/**/*.scss'],
            syntax: 'scss'
        }),
        new VueLoaderPlugin(),
        new CopyWebpackPlugin([{
            from: './assets/img/',
            to: './img'
        }, {
            from: './assets/fonts/',
            to: './fonts'
        }])
    ],
    optimization: {
        minimizer: [
            new UglifyJsPlugin(),
            new OptimizeCSSAssetsPlugin({})
        ],
        usedExports: true,
        sideEffects: true,
        splitChunks: {
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    module: {
        rules: [
            { 
                enforce: 'pre', 
                test: /\.(js)$/, 
                exclude: /node_modules/ 
            },
            {
                enforce: 'pre',
                test: /\.(js|vue)$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                         presets: [['@babel/preset-env', {
                             "shippedProposals": true
                         }]],
                         plugins: ['lodash', 'syntax-dynamic-import']
                    }
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                exclude: /node_modules/,
                options: {
                    loaders: {
                        js: {
                           loader: 'babel-loader',
                           options: {
                                presets: [['@babel/preset-env', {
                                    "shippedProposals": true
                                }]],
                                plugins: ['lodash', 'syntax-dynamic-import']
                           }
                        },
                    }
                }
            },
            {
                test: /\.html$/,
                use: [
                    {loader: 'underscore-template-loader'}
                ]
            },
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    {loader: MiniCssExtractPlugin.loader},
                    {loader: 'css-loader?url=false'},
                    {loader: 'postcss-loader', options: {
                        config: {
                            ctx: {
                                cssnano: {},
                                autoprefixer: {}
                            }
                        }
                    }},
                    {loader: 'sass-loader'}
                ]
            }
        ]
    },
    resolve: {
        unsafeCache: true,
        extensions: ['.js', '.vue', '.scss', '.css'],
        alias: {
            '@': path.resolve('assets/js/main/'),
        }
    },
    externals: [
        'canvas',
    ],
};
