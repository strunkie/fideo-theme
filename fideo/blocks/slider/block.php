<section class="section bg--white slider-container">
    <div class="grid-container">
        <div class='grid-x grid-padding-x'>
            <div class="cell">
                <div class="slider">
                    <div class="slider__slides">
                        <?php foreach($atts['images'] as $slide) : ?>
                            <div class="slider__slide">
                                <img src="<?php echo $slide['url']; ?>">
                                <div class="slider__caption">
                                    <?php echo $slide['text']; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <?php if(count($atts['images']) > 1) : ?>
                    <div class="sliderbuttons">
                        <div class="sliderbuttons__button slider__button--prev"><i class="icon-arrow-back"></i></div>
                        <div class="sliderbuttons__button slider__button--next"><i class="icon-arrow-forward"></i></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>