<?php
function slider_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "images" => array(),
    ), $atts);

    include(locate_template('blocks/slider/block.php'));

    return ob_get_clean();
}
add_shortcode('button', 'slider_shortcode'); 


function sliderInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/slider', array(
  'attributes' => array(
   'images' => array('type' => 'array'),
  ),
  'render_callback' => 'slider_shortcode'
 ) );
}
add_action( 'init', 'sliderInit' );