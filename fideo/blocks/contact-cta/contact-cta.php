<?php
function contactCta_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
    	"title" => "Bel mij terug",
    	"text" => "dan kunnen we je helpen 😃",
    	"textbuttontext" => "Neem contact op",
    	"buttontext" => "Bel mij terug",
    	"textbuttonlink" => "/contact",
    	"buttonlink" => "/contact",
    ), $atts);

    include(locate_template('blocks/contact-cta/block.php'));

    return ob_get_clean();
}
add_shortcode('contactCtablock', 'contactCta_shortcode'); 


function contactCtaInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/contact-cta', array(
  'attributes' => array(
  	'title' => array('type' => 'string'),
  	'text' => array('type' => 'string'),
  	'textbuttontext' => array('type' => 'string'),
  	'buttontext' => array('type' => 'string'),
  	'textbuttonlink' => array('type' => 'string'),
  	'buttonlink' => array('type' => 'string'),
  ),
  'render_callback' => 'contactCta_shortcode'
 ) );
}
add_action( 'init', 'contactCtaInit' );
