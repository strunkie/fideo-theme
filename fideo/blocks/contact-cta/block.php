<section class="section bg--light-grey contact-cta">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <h2 class="h3"><?php echo $atts['title'] ?></h2>

                <p>
                    <a href="<?php echo $atts['textbuttonlink'] ?>" class="btn-text"><?php echo $atts['textbuttontext'] ?></a>
                    <?php echo $atts['text'] ?>
                </p>

                <div class="contact-cta__btn">
                    <a href="<?php echo $atts['buttonlink'] ?>" class="btn">
                        <span><?php echo $atts['buttontext'] ?></span>
                        <i class="icon-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>