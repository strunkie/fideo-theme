<?php
function youtube_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "videoid" => '',
    ), $atts);

    include(locate_template('blocks/youtube/block.php'));

    return ob_get_clean();
}
add_shortcode('youtubeBlock', 'youtube_shortcode'); 


function youtubeInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/youtube', array(
  'attributes' => array(
   'videoid' => array('type' => 'string'),
  ),
  'render_callback' => 'youtube_shortcode'
 ) );
}
add_action( 'init', 'youtubeInit' );