<div class="responsive-embed widescreen video-content">
	<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $atts['videoid'] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
