<?php
function widgetvideo_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "text" => '',
        "label" => '',
        "image" => '',
        "youtubeid" => '',
    ), $atts);

    include(locate_template('blocks/widgetvideo/block.php'));

    return ob_get_clean();
}
add_shortcode('widgetvideo', 'widgetvideo_shortcode'); 


function widgetvideoInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/widget-video', array(
  'attributes' => array(
   'text' => array('type' => 'string'),
   'label' => array('type' => 'string'),
   'image' => array('type' => 'string'),
   'youtubeid' => array('type' => 'string'),
  ),
  'render_callback' => 'widgetvideo_shortcode'
 ) );
}
add_action( 'init', 'widgetvideoInit' );
