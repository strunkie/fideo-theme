<div class="widget widgetvideo">
    <a href="#" video-popup="<?php echo $atts['youtubeid']; ?>">
        <figure class="widgetvideo__figure">
            <img src="<?php echo $atts['image']; ?>">
            <div class="widgetvideo__inner">
                <div class="widget__play play">
                    <div class="play__btn">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/icons/play.png" />
                    </div>
                    <div class="label"><?php echo $atts['label']; ?></div>
                </div>
            </div>
        </figure>
    </a>
    <div class="widgetvideo__title">
        <?php echo $atts['text']; ?>
    </div>
</div>