<section class="section bg--white slider-container">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <div class="slider">
                    <div class="slider__slides">
                    	<?php foreach($atts['items'] as $index => $item) : ?>
                        <div class="slider__slide slider__slide--grey-panel">
                            <div class="step">
                                <div class="step__inner">
                                    <div>
                                        <div class="step__number"><?php echo $index + 1; ?></div>
                                        <div class="step__content">
                                            <h3 class="h2"><span class="step__number-small"><?php echo $index + 1; ?> </span><?php echo $item['title']; ?></h3>
                                            <p>
                                                <?php echo $item['text']; ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    	<?php endforeach; ?>
                    </div>

                    <div class="sliderbuttons">
                        <div class="sliderbuttons__button slider__button--prev"><i class="icon-arrow-back"></i></div>
                        <div class="sliderbuttons__button slider__button--next"><i class="icon-arrow-forward"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>