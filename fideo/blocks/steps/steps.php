<?php
function steps_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "items" => array()
    ), $atts);

    include(locate_template('blocks/steps/block.php'));

    return ob_get_clean();
}
add_shortcode('stepsBlock', 'steps_shortcode'); 


function stepsInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/steps', array(
  'attributes' => array(
   'items' => array('type' => 'array'),
  ),
  'render_callback' => 'steps_shortcode'
 ) );
}
add_action( 'init', 'stepsInit' );