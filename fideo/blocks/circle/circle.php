<?php
function circle_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "text" => '',
        "btntext" => '',
        "btnlink" => '',
    ), $atts);

    include(locate_template('blocks/circle/block.php'));

    return ob_get_clean();
}
add_shortcode('circleBlock', 'circle_shortcode'); 


function circleInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/circle', array(
  'attributes' => array(
   'text' => array('type' => 'string'),
   'btntext' => array('type' => 'string'),
   'btnlink' => array('type' => 'string')
  ),
  'render_callback' => 'circle_shortcode'
 ) );
}
add_action( 'init', 'circleInit' );