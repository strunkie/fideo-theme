<?php $current_url="https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

<section class="section bg--white">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell cell small-24 medium-8 large-6"></div>
            <div class="cell small-24 medium-15 large-14 medium-offset-1 large-offset-2 ">
                <div class="social-share">
                    <div class="social-share__title">
                        <span>Delen</span><i class="icon-share"></i>
                    </div>
                    <div class="social-share__list">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $current_url ?>" target="_blank" share>
                            <i class="icon-facebook"></i>
                        </a>
                        <a href="https://twitter.com/intent/tweet?url=<?php echo $current_url ?>" target="_blank" share>
                            <i class="icon-twitter"></i>
                        </a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $current_url ?>" target="_blank" share>
                            <i class="icon-linkedin"></i>
                        </a>
                        <a href="mailto:?body=<?php echo $current_url ?>" target="_blank">
                            <i class="icon-email"></i>
                        </a>
                        <a href="https://wa.me/?text=<?php echo $current_url ?>" target="_blank" target="_blank" share>
                            <i class="icon-whatsapp"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>