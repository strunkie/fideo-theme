<?php
function share_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
    ), $atts);

    include(locate_template('blocks/share/block.php'));

    return ob_get_clean();
}
add_shortcode('shareblock', 'share_shortcode'); 


function shareInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/share', array(
  'attributes' => array(
  ),
  'render_callback' => 'share_shortcode'
 ) );
}
add_action( 'init', 'shareInit' );