<?php
function widgetquestion_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "text" => '',
        "title" => '',
        "btntext" => '',
        "btnlink" => '',
    ), $atts);

    include(locate_template('blocks/widgetquestion/block.php'));

    return ob_get_clean();
}
add_shortcode('widgetquestion', 'widgetquestion_shortcode'); 


function widgetquestionInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/widget-question', array(
  'attributes' => array(
   'text' => array('type' => 'string'),
   'title' => array('type' => 'string'),
   'btntext' => array('type' => 'string'),
   'btnlink' => array('type' => 'string'),
  ),
  'render_callback' => 'widgetquestion_shortcode'
 ) );
}
add_action( 'init', 'widgetquestionInit' );
