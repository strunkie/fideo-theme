<div class="widget widgetquestion">
    <h4 class="h2">
        <?php echo $atts['title']; ?>
    </h4>
    <p><?php echo $atts['text']; ?></p>
    <?php if($atts['btnlink'] && $atts['btntext']) : ?>
        <a href="<?php echo $atts['btnlink']; ?>" class="btn-text"><?php echo $atts['btntext']; ?></a>
    <?php endif; ?>
</div>