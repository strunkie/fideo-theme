<?php
function usps_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "images" => array(),
    ), $atts);

    include(locate_template('blocks/usps/block.php'));

    return ob_get_clean();
}
add_shortcode('button', 'usps_shortcode'); 


function uspsInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/usps', array(
  'attributes' => array(
   'images' => array('type' => 'array'),
  ),
  'render_callback' => 'usps_shortcode'
 ) );
}
add_action( 'init', 'uspsInit' );