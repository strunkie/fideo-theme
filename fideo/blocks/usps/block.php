<?php
    $fideo_options = get_option( 'fideo_option_name' );
?>
<section class="benefits bg--<?php echo $fideo_options['usp_1_kleur_7'] ?>">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <div class="benefits__inner">
                    <div>
                        <h2 class="benefits__title">
                            <?php echo $fideo_options['usp_titel_16']; ?>
                        </h2>
                        <div class="benefits__usps h1">
                            <?php if($fideo_options['usp_1_6']) : ?>
                            <div class="benefits__text" background-class="bg--<?php echo $fideo_options['usp_1_kleur_7'] ?>">
                                <?php echo nl2br($fideo_options['usp_1_6']); ?>
                            </div>
                            <?php endif; ?>
                            <?php if($fideo_options['usp_2_8']) : ?>
                            <div class="benefits__text" background-class="bg--<?php echo $fideo_options['usp_2_kleur_9'] ?>">
                                <?php echo nl2br($fideo_options['usp_2_8']); ?>
                            </div>
                            <?php endif; ?>
                            <?php if($fideo_options['usp_3_10']) : ?>
                            <div class="benefits__text" background-class="bg--<?php echo $fideo_options['usp_3_kleur_11'] ?>">
                                <?php echo nl2br($fideo_options['usp_3_10']); ?>
                            </div>
                            <?php endif; ?>
                            <?php if($fideo_options['usp_4_12']) : ?>
                            <div class="benefits__text" background-class="bg--<?php echo $fideo_options['usp_4_kleur_13'] ?>">
                                <?php echo nl2br($fideo_options['usp_4_12']); ?>
                            </div>
                            <?php endif; ?>
                            <?php if($fideo_options['usp_5_14']) : ?>
                            <div class="benefits__text" background-class="bg--<?php echo $fideo_options['usp_5_kleur_15'] ?>">
                                <?php echo nl2br($fideo_options['usp_5_14']); ?>
                            </div>
                            <?php endif; ?>
                        </div>
                        <a href="<?php echo get_option('fideo_option_name')['usp_button_link_18'] ?>" class="btn"><span><?php echo get_option('fideo_option_name')['usp_button_tekst_17'] ?></span><i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>