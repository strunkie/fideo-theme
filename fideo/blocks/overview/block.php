<section class="section bg--<?php echo $atts['backgroundColor'] ?>">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell">
                <div class="heading heading--underline">
                    <h2 class="h3 heading__title"><?php echo $atts['title']; ?></h2>
                    <?php if ($atts['buttonStyle'] == 2) : ?>
                    <a href="<?php echo $atts['buttonLink']; ?>" title="<?php echo $atts['buttonText']; ?>" class="btn-text heading__button"><?php echo $atts['buttonText']; ?></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="cell">
                <div class="grid-x align-right">
                    <div class="cell medium-14">
                        <ul class="faq">
                            <?php
                                $args = array( 'post_type' => $atts['contentType'], 'posts_per_page' => -1 );

                                $firstPost = true;

                                $loop = new WP_Query( $args );
                                while ( $loop->have_posts() ) : $loop->the_post();
                                    ?>
                                    <li class="faq__item <?php if($firstPost) : ?>faq__item--open <?php endif; ?>">
                                        <button class="faq__button">
                                            <div class="faq__link h2"><?php the_title(); ?></div>
                                            <div class="faq__icon">
                                                <i class="icon-arrow">
                                                    <span></span>
                                                    <span></span>
                                                </i>
                                            </div>
                                        </button>
                                        <div class="faq__content">
                                            <div class="faq__content__inner">
                                                <p>
                                                    <?php echo the_field('overview_text'); ?>
                                                </p>
                                                <a href="<?php the_permalink(); ?>" class="btn-text">Lees meer</a>
                                            </div>
                                        </div>
                                    </li>
                                    <?php
                                    $firstPost = false;
                                endwhile;
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>