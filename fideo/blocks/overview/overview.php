<?php
function overview_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "title" => '',
        "contentType" => 'jobs',
        "buttonStyle" => '1',
        "buttonText" => 'Bekijk meer',
        "buttonLink" => '/',
        "backgroundColor" => 'white'
    ), $atts);

    include(locate_template('blocks/overview/block.php'));

    return ob_get_clean();
}
add_shortcode('button', 'overview_shortcode'); 


function overviewInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/overview', array(
  'attributes' => array(
   'title' => array('type' => 'string'),
   'contentType' => array('type' => 'string'),
   'nrOfItems' => array('type' => 'integer'),
   'buttonStyle' => array('type' => 'integer'),
   'buttonText' => array('type' => 'string'),
   'buttonLink' => array('type' => 'string'),
   'backgroundColor' => array('type' => 'string')
  ),
  'render_callback' => 'overview_shortcode'
 ) );
}
add_action( 'init', 'overviewInit' );