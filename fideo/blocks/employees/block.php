<section class="section bg--white employees-slider-container">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell">
                <div class="heading heading--spacing-large">
                    <h2 class="h3 heading__title"><?php echo $atts['title']; ?></h2>
                    <div class="sliderbuttons">
                        <div class="sliderbuttons__button sliderbuttons__button--prev"><i class="icon-arrow-back"></i></div>
                        <div class="sliderbuttons__button sliderbuttons__button--next"><i class="icon-arrow-forward"></i></div>
                    </div>
                </div>
            </div>
            <div class="cell">
                <div class="employees-slider">
                    <?php
                        $args = array( 'post_type' => 'employees', 'posts_per_page' => -1 );

                        $firstPost = true;

                        $loop = new WP_Query( $args );
                        while ( $loop->have_posts() ) : $loop->the_post();
                        $video = get_field('photo_gif')['url'];
                    ?>
                    <div class="employees" hover-video>
                        <figure class="employees__figure">
                            <?php if($video) { ?>
                            <div class="employees__figure-video">
                                <video width="530" height="780" loop muted>
                                    <source src="<?php echo $video ?>" type="video/mp4">
                                </video>
                            </div>
                            <?php } ?>

                            <img src="<?php echo get_field('photo')['sizes']['employee'] ?>" alt="<?php echo the_title() ?>">
                        </figure>
                        <div class="employees__undertitle"><?php echo the_field('jobtitle') ?></div>
                        <h3 class="h2 employees__title"><?php echo the_title() ?></h3>
                        <div class="employees__contact">
                            <a href="tel:<?php echo the_field('phonenumber') ?>">M <?php echo the_field('phonenumber') ?></a><br>
                            <a href="mailto:<?php echo the_field('mailaddress') ?>">E <?php echo the_field('mailaddress') ?></a>
                        </div>
                    </div>
                    <?php
                        endwhile;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>