<?php
function employees_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "title" => "",
    ), $atts);

    include(locate_template('blocks/employees/block.php'));

    return ob_get_clean();
}
add_shortcode('employees', 'employees_shortcode'); 


function employeesInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/employees', array(
  'attributes' => array(
   'title' => array('type' => 'string'),
  ),
  'render_callback' => 'employees_shortcode'
 ) );
}
add_action( 'init', 'employeesInit' );