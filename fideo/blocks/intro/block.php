<section class="section bg--white">
    <div class="grid-container">
        <div class="grid-x grid-padding-x align-center">
            <div class="cell small-24 medium-20">
                <div class="intro-section">
                    <div class="h3"><?php echo $atts['text'] ?></div>

                    <?php if(!empty($atts['btntext']) && !empty($atts['btnlink'])) : ?>
                    <a href="<?php echo $atts['btnlink'] ?>" class="btn">
                        <span><?php echo $atts['btntext'] ?></span>
                        <i class="icon-arrow-right"></i>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
