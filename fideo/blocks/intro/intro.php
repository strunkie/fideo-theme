<?php
function intro_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "text" => '',
        "btntext" => '',
        "btnlink" => '',
    ), $atts);

    include(locate_template('blocks/intro/block.php'));

    return ob_get_clean();
}
add_shortcode('introBlock', 'intro_shortcode'); 


function introInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/intro', array(
  'attributes' => array(
   'text' => array('type' => 'string'),
   'btntext' => array('type' => 'string'),
   'btnlink' => array('type' => 'string')
  ),
  'render_callback' => 'intro_shortcode'
 ) );
}
add_action( 'init', 'introInit' );