<?php
function linklist_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "text" => "",
        "href" => "",
    ), $atts);

    include(locate_template('blocks/linklist/block.php'));

    return ob_get_clean();
}
add_shortcode('button', 'linklist_shortcode'); 


function linklistInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/linklist', array(
  'attributes' => array(
   'text' => array('type' => 'string'),
   'href' => array('type' => 'string'),
  ),
  'render_callback' => 'linklist_shortcode'
 ) );
}
add_action( 'init', 'linklistInit' );