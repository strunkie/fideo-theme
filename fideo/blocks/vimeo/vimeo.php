<?php
function vimeo_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "videoid" => '',
    ), $atts);

    include(locate_template('blocks/vimeo/block.php'));

    return ob_get_clean();
}
add_shortcode('vimeoBlock', 'vimeo_shortcode'); 


function vimeoInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/vimeo', array(
  'attributes' => array(
   'videoid' => array('type' => 'string'),
  ),
  'render_callback' => 'vimeo_shortcode'
 ) );
}
add_action( 'init', 'vimeoInit' );