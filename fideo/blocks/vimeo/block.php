<div class="responsive-embed widescreen video-content">
    <iframe src="https://player.vimeo.com/video/<?php echo $atts['videoid'] ?>" width="320" height="240" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>