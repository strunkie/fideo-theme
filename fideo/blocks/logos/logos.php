<?php
function logos_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
    ), $atts);

    include(locate_template('blocks/logos/block.php'));

    return ob_get_clean();
}
add_shortcode('button', 'logos_shortcode'); 


function logosInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/logos', array(
  'attributes' => array(
  ),
  'render_callback' => 'logos_shortcode'
 ) );
}
add_action( 'init', 'logosInit' );