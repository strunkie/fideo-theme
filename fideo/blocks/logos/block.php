<section class="section section--spacing-large bg--white partners">
    <div class="partners__inner">
        <div class="partners__panel">
            <?php
            $args = array( 'post_type' => 'partners', 'posts_per_page' => -1 );

            $loop = new WP_Query( $args );
            while ( $loop->have_posts() ) : $loop->the_post();
                ?>
                <div class="logo">
                    <?php if(get_field('link')) : ?>
                    <a href="<?php echo get_field('link'); ?>" class="logo__inner">
                        <img class="partners__image" src="<?php echo get_field('image')['sizes']['partner']; ?>" alt="<?php echo the_title(); ?>">
                    </a>
                    <?php else : ?>
                    <div class="logo__inner">
                        <img class="partners__image" src="<?php echo get_field('image')['sizes']['partner']; ?>" alt="<?php echo the_title(); ?>">
                    </div>
                    <?php endif; ?>
                </div>
                <?php
            endwhile;
            ?>
        </div>
    </div>
</section>