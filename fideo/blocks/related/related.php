<?php
function relatedBlock_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "title" => '',
        "contenttype" => 'cases',
        "nrofitems" => '2',
        "buttonstyle" => '1',
        "buttontext" => 'Bekijk meer',
        "buttonlink" => '/',
        "selectedItems" => array()
    ), $atts);

    include(locate_template('blocks/related/block.php'));

    return ob_get_clean();
}
add_shortcode('related', 'relatedBlock_shortcode'); 


function relatedBlockInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/related', array(
  'attributes' => array(
   'title' => array('type' => 'string'),
   'contenttype' => array('type' => 'string'),
   'nrofitems' => array('type' => 'integer'),
   'buttonstyle' => array('type' => 'integer'),
   'buttontext' => array('type' => 'string'),
   'buttonlink' => array('type' => 'string'),
   'selectedItems' => array('type' => 'array')
  ),
  'render_callback' => 'relatedBlock_shortcode'
 ) );
}
add_action( 'init', 'relatedBlockInit' );