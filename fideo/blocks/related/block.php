<section class="section bg--white articles">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-24">
                <div class="heading">
                    <h2 class="h3 heading__title"><?php echo $atts['title']; ?></h2>
                    <?php if ($atts['buttonstyle'] == 2) : ?>
                    <a href="<?php echo $atts['buttonlink']; ?>" title="<?php echo $atts['buttontext']; ?>" class="btn-text heading__button"><?php echo $atts['buttontext']; ?></a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="cell small-24">
                <div class="grid-x grid-padding-x grid-margin-large-y small-up-1 medium-up-2 large-up-<?php echo $atts['nrofitems'] ?>">
                    <?php
                        global $post;
                        $post_slug=$post->post_name;

                        $args = array( 'post_type' => $atts['contenttype'], 'posts_per_page' => 40, 'orderby' => 'rand' );
                        $foundOne = false;
                        $index = 0;
                        $loop = new WP_Query( $args );

                        $post_ids = wp_list_pluck( $loop->posts, 'ID' );

                        foreach($post_ids as $postid) {
                            $slug = get_post_field( 'post_name', $postid );

                            if($post_slug === $slug) {
                                if (($key = array_search($postid, $post_ids)) !== false) {
                                    unset($post_ids[$key]);
                                    $post_ids = array_values(array_filter($post_ids));
                                }
                            }
                        }

                        // removed preselected items from query selector
                        foreach($atts['selectedItems'] as $item) {
                            if (($key = array_search($item, $post_ids)) !== false) {
                                unset($post_ids[$key]);
                                $post_ids = array_values(array_filter($post_ids));
                            }
                        }

                        for ($itemindex = 0; $itemindex < $atts['nrofitems']; $itemindex++) {
                            // check if slot is selected by user
                            if(!empty($atts['selectedItems'][$itemindex]) && $atts['selectedItems'][$itemindex] != -1) {
                                $post_id = $atts['selectedItems'][$itemindex];
                            } else {
                                $post_id = $post_ids[0];
                                unset($post_ids[0]);
                                $post_ids = array_values(array_filter($post_ids));
                            }

                            global $post; 
                            $post = get_post( $post_id, OBJECT );
                            setup_postdata( $post );

                            get_template_part('article-' . $atts['contenttype']);

                            wp_reset_postdata();
                        }
                    ?>
                </div>
            </div>
            <?php if ($atts['buttonstyle'] == 3) : ?>
            <div class="cell small-24 article__button">
                <a href="<?php echo $atts['buttonlink']; ?>" title="<?php echo $atts['buttontext']; ?>" class="btn">
                    <span><?php echo $atts['buttontext']; ?></span>
                    <i class="icon-arrow-right"></i>
                </a>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>