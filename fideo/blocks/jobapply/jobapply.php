<?php
function jobapply_shortcode($atts) {
    ob_start();

    $atts = shortcode_atts(array(
        "text" => '',
        "btntext" => '',
        "btnlink" => '',
    ), $atts);

    include(locate_template('blocks/jobapply/block.php'));

    return ob_get_clean();
}
add_shortcode('jobapplyBlock', 'jobapply_shortcode'); 


function jobapplyInit() {
 if ( ! function_exists( 'register_block_type' ) ) {
  return;
 }
register_block_type( 'cgb/jobapply', array(
  'attributes' => array(
   'text' => array('type' => 'string'),
   'btntext' => array('type' => 'string'),
   'btnlink' => array('type' => 'string')
  ),
  'render_callback' => 'jobapply_shortcode'
 ) );
}
add_action( 'init', 'jobapplyInit' );