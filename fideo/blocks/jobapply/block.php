<section class="section bg--light-grey">
    <div class="grid-container">
        <div class="grid-x grid-padding-x">
            <div class="cell small-24 large-14 large-offset-8">

                <h2 class="h3 heading heading--spacing-large">Solliciteer direct</h2>

                <form class="form form--bg-light-grey form--spacing-large ajaxform" method="post" action="/wp-json/fideo/v1/jobapply/" id="form1" novalidate ajaxform>

                    <div class="grid-x grid-padding-x">
                        <div class="cell small-24 medium-12 large-12">
                            <div class="formfield formfield--input">
                                <label class="formfield__label">Mijn voornaam is</label>
                            
                                <input type="text" placeholder="voornaam..." name="firstname" fieldID="firstname">
                            
                                <ul class="form__errors" data-errorgroup="firstname">
                                </ul>
                            </div>
                        </div>
                        <div class="cell small-24 medium-12 large-12">
                            <div class="formfield formfield--input">
                                <label class="formfield__label">Mijn achternaam is</label>
                            
                                <input type="text" placeholder="achternaam..." name="lastname" fieldID="lastname">
                            
                                <ul class="form__errors" data-errorgroup="lastname"> 
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="grid-x grid-padding-x">
                        <div class="cell small-24 medium-12 large-12">
                            <div class="formfield formfield--input">
                                <label class="formfield__label">Mijn emailadres is</label>
                            
                                <input type="text" placeholder="emailadres..." name="email" fieldID="email">
                            
                                <ul class="form__errors" data-errorgroup="email"> 
                                </ul>
                            </div>
                        </div>
                        <div class="cell small-24 medium-12 large-12">
                            <div class="formfield formfield--input">
                                <label class="formfield__label">Mijn telefoonnummer is</label>
                            
                                <input type="text" placeholder="telefoonnummer..." name="phonenumber" fieldID="phonenumber">
                            
                                <ul class="form__errors" data-errorgroup="phonenumber"> 
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="grid-x grid-padding-x">
                        <div class="cell small-24">
                            <div class="formfield formfield--textarea">
                                <label class="formfield__label">Mijn motivatie</label>
                            
                                <textarea placeholder="type een korte motivatie" name="motivation" fieldID="motivation"></textarea>

                                <ul class="form__errors" data-errorgroup="motivation"> 
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="grid-x grid-padding-x">
                        <div class="cell small-24">
                            <div class="formfield">
                                <label class="formfield__label">(zip)bestand selecteren...(max. 15MB)</label>
                                
                                <ul class="form__errors" data-errorgroup="attachment"> </ul>
                                
                                <div class="formfield formfield--file">
                                    <input type="file" name="attachment" placeholder="Bestand selecteren">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="grid-x grid-padding-x">
                        <div class="cell small-24">
                            <div class="formfield formfield--inputlist">                                
                                <ul>
                                    <li>
                                        <label class="formfield__choice">
                                            <input type="checkbox" name="av" fieldID="av">
                                            <span>Ik ga akkoord met de <a href="<?php echo get_privacy_policy_url(); ?>" target="_blank">privacy statement<a/></span>
                                        </label>
                                    </li>
                                </ul>

                                <ul class="form__errors" data-errorgroup="newsletter av"> 
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="formfield formfield--button">
                        <button>
                            <div class="btn">
                                <span>Solliciteer</span>
                                <i class="icon-arrow-right"></i>
                            </div>
                        </button>
                
                        <span class="loader loader--yellow"></span>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>