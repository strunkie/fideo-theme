<?php
    $categories = wp_get_post_terms(get_the_ID(), 'category', array("fields" => "all"));
    $video = get_field('highlight_afbeelding_gif')['url'];
?>

<a class="cell article" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" <?php if($video) { ?>hover-video<?php } ?>>
    <figure class="article__figure">
    	<?php if($video) { ?>
        <div class="article__figure-video">
            <video width="720" height="480" loop muted>
                <source src="<?php echo get_field('highlight_afbeelding_gif')['url'] ?>" type="video/mp4">
            </video>
        </div>
        <?php } ?>

        <img class="article__figure__image" src="<?php echo get_field('highlight_afbeelding_static')['sizes']['overview-image'] ?>" alt="<?php the_title(); ?>">
    </figure>
    <?php if(get_field('overview_subtitle')) : ?>
        <div class="article__undertitle"><?php echo get_field('overview_subtitle'); ?></div>
    <?php endif; ?>
    <h3 class="h2 article__title"><?php echo get_field('alternative_title') ?: the_title(); ?></h3>
</a>