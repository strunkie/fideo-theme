<?php get_header(); ?>
	<!-- header -->
	<header class="header header--video">
        <div class="bg-video" video="<?php echo get_field('header_youtubeid'); ?>">
            <div class="bg-video__cover <?php if(get_field('show_pattern')) { echo 'bg-video__cover--pattern'; } ?>"></div>
            <div id="tv">
                
            </div>
        </div>
        
        <?php
            if( get_field('mobile_vid') && get_field('mobile_vid')['url'] ) :
        ?>
        <div class="bg-video-mobile">
            <video
                id="heroVideoMobile"
                src="<?php echo get_field('mobile_vid')['url'] ?>"
                playsinline
                autoplay
                muted
                loop
            >
            </video>
        </div>

        <?php
            endif;
        ?>

        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--white">
                            <?php the_title(); ?>
                        </h1>
                        
                        <div class="header__content">
                            <?php if(get_field('header_sub_title')): ?>
							<div class="h5 header__subtitle">
                                <?php echo get_field('header_sub_title'); ?>
                            </div>
                            <?php endif; ?>

                            <?php if(get_field('header_sub')): ?>
							<p class="header__text">
								<?php echo get_field('header_sub'); ?>
							</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php if(get_field('header_youtubeid_button') && get_field('header_buttontext')) : ?>
            <div class="header__link">
                <a href="#" class="btn-text" video-popup="<?php echo get_field('header_youtubeid_button'); ?>"><?php echo get_field('header_buttontext') ?></a>
            </div>
        <?php endif; ?>
    </header>
	<!-- /header -->


	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
	<?php endif; ?>
<?php get_footer(); ?>
