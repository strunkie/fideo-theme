<?php /* Template Name: Bedankt pagina */ get_header(); ?>
	<!-- header -->
	<header class="header header--video">
        <div class="bg-video" video="<?php echo get_field('header_youtubeid'); ?>">
            <div class="bg-video__cover <?php if(get_field('show_pattern')) { echo 'bg-video__cover--pattern'; } ?>"></div>
            <div id="tv"></div>
        </div>

        <?php
            if( get_field('mobile_vid') && get_field('mobile_vid')['url'] ) :
        ?>
        <div class="bg-video-mobile">
            <video
                id="heroVideoMobile"
                src="<?php echo get_field('mobile_vid')['url'] ?>"
                playsinline
                autoplay
                muted
                loop
            >
            </video>
        </div>

        <?php
            endif;
        ?>

        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--white">
                            <?php the_title(); ?>
                        </h1>

                        <div class="header__content">
                            <?php if(get_field('header_sub_title')): ?>
							<div class="h5 header__subtitle">
                                <?php echo get_field('header_sub_title'); ?>
                            </div>
                            <?php endif; ?>

                            <?php if(get_field('header_sub')): ?>
							<p class="header__text">
								<?php echo get_field('header_sub'); ?>
							</p>
                            <?php endif; ?>

                            <a href="/" class="btn btn--yellow-red"><span>Terug naar home</span><i class="icon-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
	<!-- /header -->

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
