			<!-- footer -->
		<?php global $nofooter; ?>
		<?php if(empty($nofooter) || $nofooter === false) : ?>
		<footer class="footer">
		    <div class="grid-container">
		        <div class="grid-x grid-padding-x">
		            <div class="cell small-24 medium-12 footer__content">
		                <div class="footer__content__inner">
		                    <nav class="footer__navigation">
		                        <ul>
		                            <?php foreach(wp_get_nav_menu_items('Footer') as $item) : ?>
		                            	<li><a href="<?php echo $item->url; ?>"><?php echo $item->title ?></a></li>
		                            <?php endforeach; ?>
		                        </ul>
		                    </nav>
		                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logos/logo-fideo.svg" alt="Fideo" class="footer__logo">
		                    <div class="social-icons">
		                        <a href="https://www.facebook.com/fideonl/" target="_blank">
		                            <i class="icon-facebook"></i>
		                        </a>
		                        <a href="https://www.youtube.com/channel/UCaWVI9d9IY7eCkENF7BRMXg" target="_blank">
		                            <i class="icon-youtube"></i>
		                        </a>
		                        <a href="https://www.instagram.com/fideonl/" target="_blank">
		                            <i class="icon-instagram"></i>
		                        </a>
		                        <a href="https://www.linkedin.com/company/fideo/" target="_blank">
		                            <i class="icon-linkedin"></i>
		                        </a>
		                    </div>
		                </div>
		            </div>
		            <div class="cell small-24 medium-12 footer__content footer__content--aside bg--red">
		                <div class="footer__content__inner">
		                    <div class="footer__cta">
		                        <h3 class="h3 h--blue footer__cta__title">
		                        	<?php echo get_option('fideo_option_name')['nieuwsbrief_titel_5'] ?>
		                        </h3>

		                        <div class="newsletter">
		                        	<div class="newsletter__step" intro>
		                        		<a href="#" class="btn-text" footer-newsletter="start">Ik wil de nieuwsbrief ontvangen</a>
		                        	</div>
		                        	<div class="newsletter__step" form>
		                        		<form class="form form--bg-red ajaxform" method="post" action="/wp-json/fideo/v1/newsletter/" id="form1" novalidate newsletter>
				                        	<div class="formfield formfield--input">					                        	
				                        	    <input type="email" placeholder="E-mailadres..." name="email" fieldID="email">
				                        	
				                        	    <ul class="form__errors" data-errorgroup="email"> 
				                        	    </ul>
				                        	</div>

				                        	<div class="formfield formfield--choice">
				                        		<label class="formfield__choice">
				                        		    <input type="checkbox" name="privacy" fieldID="privacy">
				                        		    <span>Ik ga akkoord met de <a target="_blank" href="<?php echo get_privacy_policy_url(); ?>">privacy statement</a></span>
				                        		</label>

				                        	    <ul class="form__errors" data-errorgroup="privacy"> 
				                        	    </ul>
				                        	</div>

				                        	<div class="formfield formfield--button">
	                                            <button>
	                                                <div class="btn">
	                                                    <span>Aanmelden</span>
	                                                    <i class="icon-arrow-right"></i>
	                                                </div>
	                                            </button>
	                                    
	                                        	<span class="loader loader--white"></span>
	                                        </div>
				                        </form>
		                        	</div>
		                        	<div class="newsletter__step" success>
		                        		<p><?php echo get_option('fideo_option_name')['nieuwsbrief_bedanktmelding_5'] ?></p>
		                        	</div>
		                        </div>

		                    </div>
		                    <div class="footer__disclaimer">
		                    	<?php foreach(wp_get_nav_menu_items('Footer sub') as $item) : ?>
		                    		<a href="<?php echo $item->url; ?>"><?php echo $item->title ?></a>
		                    	<?php endforeach; ?>
		                        
		                        <span>&copy; <?php echo date("Y"); ?> Fideo</span>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</footer>
		<?php endif; ?>
			<!-- /footer -->

		</div>

		<div class="video-popup">
			<div class="video-popup__bg" close-video-popup></div>
			<div class="video-popup__inner">
				<a class="video-popup__close" close-video-popup href="#">
					<i class="icon-close"></i>
				</a>
				<div class="responsive-embed widescreen">
					<iframe width="560" height="315" src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
		<!-- /wrapper -->

		<?php wp_footer(); ?>

	</body>
</html>
