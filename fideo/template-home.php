<?php /* Template Name: Home Template */ get_header(); ?>
	<!-- header -->
	<header class="header header--home" style="background-image: url(<?php echo get_field('alternative_image')['sizes']['header'] ?>)">
        <div class="bg-video" video="<?php echo get_field('header_youtubeid'); ?>">
            <div class="bg-video__cover <?php if(get_field('show_pattern')) { echo 'bg-video__cover--pattern'; } ?>"></div>
            <div id="tv"></div>
        </div>
        
        <?php
            if( get_field('mobile_vid') && get_field('mobile_vid')['url'] ) :
        ?>
        <div class="bg-video-mobile">
            <video
                id="heroVideoMobile"
                src="<?php echo get_field('mobile_vid')['url'] ?>"
                playsinline
                autoplay
                muted
                loop
            >
            </video>
        </div>

        <?php
            endif;
        ?>

        <div class="header__inner">
            <div class="grid-container">
                <div class="grid-x grid-padding-x">
                    <div class="cell">
                        <h1 class="h--uppercase h--red">
                            <?php the_title(); ?>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        
        <?php if(get_field('header_youtubeid_button') && get_field('header_buttontext')) : ?>
            <div class="header__link">
                <a href="#" class="btn-text" video-popup="<?php echo get_field('header_youtubeid_button'); ?>"><?php echo get_field('header_buttontext') ?></a>
            </div>
        <?php endif; ?>
    </header>
	<!-- /header -->

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content(); // Dynamic Content ?>
	<?php endwhile; ?>
	<?php endif; ?>

<?php get_footer(); ?>
